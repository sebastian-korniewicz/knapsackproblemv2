﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.generator;
using KnapsackProblem.data.models;
using KnapsackProblem.algorithms;
using KnapsackProblem.algorithms.Combo;
using KnapsackProblem.algorithms.heuristic;
using KnapsackProblem.benchmark;
using KnapsackProblem.data.validatorResult;
using KnapsackProblem.validatorResult;

namespace ConsoleProgram
{
   
    class Program
    {
        
        static void Main(string[] args)
        {
            DynamicProgramming dynamicProgramming = new DynamicProgramming();
            DynamicProgrammingOpt dynamicProgrammingOpt = new DynamicProgrammingOpt();
            Minknap minknap = new Minknap();
            KSLog kSLog = new KSLog();
            Combo combo = new Combo();
            TreeMutationHeuristic treeMutationHeuristic = new TreeMutationHeuristic();
            TestSolver testSolver = new TestSolver();
            Benchmark benchmark = new Benchmark();
            
            benchmark.Memory = true;
            benchmark.TimeOut = 30;
            benchmark.CsvFile = "minknap.csv";
            benchmark.AmountOfTestsInstance = 1;
            List<int> itemAmount = new List<int> { 50000 };
            benchmark.ItemAmount = itemAmount;
            benchmark.KnapsackGenerator = new DiscreteKnapsackGeneratorPercents();
            benchmark.KnapsackValidator = new DeviationValidatorCombo();
            object[] range = { 100000, 1000000, 10000000 };
            object[] capacity = { 30d, 50d };
            object[] dataTypes =
            {
                DataType.UncorrelatedDataInstances,
                DataType.WeaklyCorrelatedInstances
            };
            benchmark.ParametersForGenerator = new List<object[]> { range, capacity, dataTypes };
            benchmark.RunTests(minknap);

           

            Console.ReadLine();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Console.ReadLine();
        }
    }
}
