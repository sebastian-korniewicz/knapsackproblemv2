﻿using KnapsackProblem.algorithms;
using KnapsackProblem.benchmark;
using KnapsackProblem.data.generator;
using KnapsackProblem.validatorResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPDynamicCapacityBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            DynamicProgramming dynamicProgramming = new DynamicProgramming();
            Benchmark benchmark = new Benchmark();
            benchmark.Memory = true;
            benchmark.TimeOut = 30;
            benchmark.CsvFile = "dp1.csv";
            benchmark.AmountOfTestsInstance = 2;
            List<int> itemAmount = new List<int> { 50, 100, 150, 200 };
            benchmark.ItemAmount = itemAmount;
            benchmark.KnapsackGenerator = new DiscreteKnapsackGeneratorPercents();
            benchmark.KnapsackValidator = new DeviationValidatorCombo();
            object[] range = { 100, 1000, 10000 };
            object[] capacity = { 30d, 50d };
            object[] dataTypes =
            {
                DataType.UncorrelatedDataInstances,
                DataType.WeaklyCorrelatedInstances
            };
            benchmark.ParametersForGenerator = new List<object[]> { range, capacity, dataTypes };
            benchmark.RunTests(dynamicProgramming);
        }
    }
}
