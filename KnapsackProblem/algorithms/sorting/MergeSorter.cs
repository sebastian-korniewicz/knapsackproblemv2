﻿using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

namespace KnapsackProblem.algorithms.sorting
{
    public class MergeSorter
    {
        private Item[] items;

        private IComparer<Item> comparer;

        public Item[] IterativeMergeSort(IList<Item> collection, IComparer<Item> comparer)
        {
            this.comparer = comparer;
            items = new Item[collection.Count];
            for (int i = 0; i < collection.Count; i++)
                items[i] = collection[i];

            for (int i = 1; i <= collection.Count - 1; i *= 2)
            {
                for (int j = i; j < collection.Count; j += 2 * i)
                {
                    merge(j - i, j, Math.Min(j + i, collection.Count));
                }
            }

            return items;
        }


        private void merge(int start, int middle, int end)
        {
            Item[] merge = new Item[end - start];
            int left = 0;
            int right = 0;
            int i = 0;

            while (left < middle - start && right < end - middle)
            {
                merge[i++] = comparer.Compare(items[start + left], items[middle + right]) < 0
                    ? items[start + left++]
                    : items[middle + right++];
            }

            while (right < end - middle)
                merge[i++] = items[middle + right++];

            while (left < middle - start)
                merge[i++] = items[start + left++];

            Array.Copy(merge, 0, items, start, merge.Length);
        }


        public int[] SortByValue(Item[] items)
        {
            int[] indexes = new int[items.Length];
            for (int i = 0; i < items.Length; i++)
                indexes[i] = i;
            
            int[] buffArray = new int[items.Length];
            for (int i = 1; i <= items.Length - 1; i *= 2)
            {
                for (int j = i; j < items.Length; j += 2 * i)
                {
                    mergeByValue(buffArray, items, indexes, j - i, j, Math.Min(j + i, items.Length));
                }
            }
            return indexes;
        }

        private void mergeByValue(int[] buffArray, Item[] items, int[] indexes, int start, int middle, int end)
        {
            int i = 0;
            int left = 0;
            int right = 0;
            while (left < middle - start && right < end - middle)
            {
                buffArray[i++] = items[indexes[start + left]].Value > items[indexes[middle + right]].Value
                    ? indexes[start + left++]
                    : indexes[middle + right++];

            }
            while (right < end - middle)
                buffArray[i++] = indexes[middle + right++];

            while (left < middle - start)
                buffArray[i++] = indexes[start + left++];

            Array.Copy(buffArray, 0, indexes, start, end - start);
        }
    }
}
