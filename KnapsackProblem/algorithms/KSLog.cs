﻿using KnapsackProblem.data.models;
using KnapsackProblem.data.models.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.algorithms.sorting;
using System.Diagnostics;
using KnapsackProblem.data.models.discreteKnapsackProblem;

namespace KnapsackProblem.algorithms
{
    public class KSLog : IKnapsackSolver
    {
        public string GetName()
        {
            return "KSLog";
        }

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public IKnapsackResult Solve(IKnapsack knapsack)
        {
            DiscreteKnapsack discreteKnapsack = (DiscreteKnapsack) knapsack;

            Comparer<Item> reverseOrderByValue = Comparer<Item>.Create(
                    (a, b) => b.Value.CompareTo(a.Value)
                );
            Comparer<Item> orderByWeight = Comparer<Item>.Create(
                    (a, b) => a.Weight.CompareTo(b.Weight)
                );

            //sortowanie
            MergeSorter mergeSorter = new MergeSorter();
            Item[] t2 = mergeSorter.IterativeMergeSort(discreteKnapsack.Items, orderByWeight);
            int[] t1 = mergeSorter.SortByValue(t2);
             
            bool[] PRG = new bool[discreteKnapsack.Items.Length];
            bool[] LEG = new bool[discreteKnapsack.Items.Length];
            bool[] BestLEG = new bool[discreteKnapsack.Items.Length];
            int bestValue = -1;
            int pocz = -1;
            int sumOfWeight = 0;
            int L = DoPRG(PRG, t2, ref sumOfWeight, pocz, (int)discreteKnapsack.Capacity);
            int last = L;
            for (int i = 0; i < L; i++)
                sumOfWeight += t2[i].Weight;
            if (L<discreteKnapsack.Items.Length)
            {
                int iter = 1;
                do
                {
                    pocz = DoPRG(PRG, t2,ref sumOfWeight, pocz, (int)discreteKnapsack.Capacity);
                    //pocz--;

                    for (int i = 0; i < L; i++)
                    {
                        LEG[discreteKnapsack.Items.Length - 1 - i] = true;
                    }
                    
                    CheckSiblings(t1, t2,ref sumOfWeight, (int)discreteKnapsack.Capacity, PRG);
                    int currentValue = GetValue(t2, PRG);
                    if (bestValue < currentValue)
                    {
                        bestValue = GetValue(t2, PRG);
                        Array.Copy(PRG, BestLEG, PRG.Length);
                    }

                    for (int i = 0; i < iter; i++)
                    {
                        GoUp(ref PRG, ref sumOfWeight, t2);
                    }

                    pocz = UpAndGetLast(ref PRG, ref sumOfWeight, t2);
                    
                    bool end = true;
                    for (int i = 0; i < PRG.Length; i++)
                        if (PRG[i])
                            end = false;
                        

                } while (pocz>-1);

            }

            List<Item> retItems = new List<Item>();
            for (int i = 0; i < discreteKnapsack.Items.Length; i++)
            {
                if (BestLEG[i])
                    retItems.Add(t2[i]);
            }

            KnapsackResult result = new KnapsackResult(retItems, (long)bestValue);

            return result;
        }

        private int GetValue(Item[] t2, bool[] lEG)
        {
            int value = 0;
            for (int i = 0; i < lEG.Length; i++)
            {
                if (lEG[i])
                    value += t2[i].Value;
            }
            return value;
        }

        private int UpAndGetLast(ref bool[] index, ref int sumOfWeight, Item[] t2)
        {
            int i = index.Length;

            while (i > 0)
                if (index[--i])
                {
                    index[i] = false;
                    sumOfWeight -= t2[i].Weight;
                    return i;
                }
            return -1;
        }

        private void CheckSiblings(int[] t1, Item[] t2,ref int sumOfWeight, int capacity, bool[] PRG)
        {
            int lastItem = PRG.Length;
            do
            {
            } while (!PRG[--lastItem]);

            //bool[] buff = new bool[LEG.Length];
            //Array.Copy(PRG,buff,LEG.Length);

            // usuwamy ostatnio dodany element
            
            sumOfWeight -= t2[lastItem].Weight;

            // dodajemy najwiekszy przedmiot
            int biggestItem = PRG.Length - 1;
            int bestIndex = biggestItem;
            // jeśli najwiekszy element sie nie miesci sprawdzamy posrednie
            if (sumOfWeight + t2[biggestItem].Weight > capacity)
            {
                int leftIndex = lastItem;
                int rightIndex = biggestItem;
                int mid = 0;
                while (leftIndex < rightIndex)
                {

                    mid = (leftIndex + rightIndex) / 2;
                    if (sumOfWeight + t2[mid].Weight <= capacity)
                    {
                        bestIndex = mid;
                        leftIndex = mid + 1;
                    }
                    else
                        rightIndex = mid;
                }
            }

            int theMostValuableSibling = -1;
            do
            {

            } while (t1[++theMostValuableSibling] > bestIndex || t1[theMostValuableSibling]<lastItem);

            PRG[lastItem] = false;
            PRG[t1[theMostValuableSibling]] = true;
            sumOfWeight += t2[t1[theMostValuableSibling]].Weight;

        }

        private bool GoUp(ref bool[] index, ref int sumOfWeight, Item[] t2)
        {
            int i = index.Length;
            
            while (i>0)
                if (index[--i])
                {
                    index[i] = false;
                    sumOfWeight -= t2[i].Weight;
                    return true;
                }
            return false;
        }
        private void Ust_LEG(ref bool[] index, bool[] LEG, int sumOfWeight)
        {
            return;
        }

        private int DoPRG(bool[] PRG, Item[] items, ref int sumOfWeight, int pocz, int capacity)
        {
            sumOfWeight = 0;
            int j = -1;
            while (j<pocz)
                if (PRG[++j])
                    sumOfWeight += items[j].Weight;
            while ((j<PRG.Length-1) && (sumOfWeight<=capacity))
            {
                sumOfWeight += items[++j].Weight;
                PRG[j] = true;
            }

            if (sumOfWeight>capacity)
            {
                sumOfWeight -= items[j].Weight;
                PRG[j--] = false;
            }

            return j - pocz;
        }

        private void PrintArray(Item[] items)
        {
            Console.WriteLine("id\tWeight\tValue");
            int i = 0;
            foreach (var item in items)
            {
                Console.WriteLine(i++ + "\t" + item.Weight + "\t" + item.Value);
            }
        }

        public void SetCustomParameters(object[] objects)
        {

        }
    }
}
