﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.algorithms
{

    using boolean = System.Int32;
    using ntype = System.Int32;            /* number of states/items   */
    using itype = System.Int32;             /* item profits and weights */
    using stype = System.Int64;             /* sum of pofit or weight   */
    using ptype = System.Double;            /* product type (sufficient precision) */
    using btype = System.UInt64;            /* binary representation of solution */
    using item = System.Int32;

    static class Constants
    {
        /// <summary>
        /// when to switch to linear scan in bins
        /// </summary>
        public const int SYNC = 5;
        public const int MAXSTATES = 400000 * 8;
        /// <summary>
        /// depth of stack used in qsort
        /// </summary>
        public const int SORTSTACK = 100;
        /// <summary>
        /// find exact median in qsort if larger size
        /// </summary>
        public const int MINMED = 100;



        public const int TRUE = 1;
        public const int FALSE = 0;

        public const int LEFT = 1;
        public const int RIGHT = 2;

        public const int PARTIATE = 1;
        public const int SORTALL = 2;

        /// <summary>
        /// number of bits in a long integer
        /// </summary>
        public const int MAXV = sizeof(btype)*8;
        /// <summary>
        /// profit of worlds most efficient item
        /// </summary>
        public const int PMAX = 1;
        /// <summary>
        /// weight of worlds most efficient item
        /// </summary>
        public const int WMAX = 0;
        /// <summary>
        /// profit of worlds least efficient item
        /// </summary>
        public const int PMIN = 0;
        /// <summary>
        /// weight of worlds least efficient item
        /// </summary>
        public const int WMIN = 1;
    }

    struct Interval
    {
        /// <summary>
        /// first item in interval
        /// </summary>
        public int f;
        /// <summary>
        /// last item in interval
        /// </summary>
        public int l;
    }

    struct State
    {
        /// <summary>
        /// profit sum
        /// </summary>
        public stype psum;
        /// <summary>
        /// weight sum
        /// </summary>
        public stype wsum;
        /// <summary>
        /// solution vector
        /// </summary>
        public btype vect;
    }

    /// <summary>
    /// set of states
    /// </summary>
    struct SetState
    {
        /// <summary>
        /// set size
        /// </summary>
        public ntype size;
        /// <summary>
        /// first element in set
        /// </summary>
        public State[] set1;
        /// <summary>
        /// first element in array
        /// </summary>
        public int fset;
        /// <summary>
        /// last element in set
        /// </summary>
        public int lset;
        /// <summary>
        /// last element in array
        /// </summary>
        public int setm;
    }

    struct ItemE
    {
        public Item Item;
        /// <summary>
        /// solution variable
        /// </summary>
        public boolean x;
    }

    public class Minknap : IKnapsackSolver
    {
        #region private variables
        /// <summary>
        /// number of items
        /// </summary>
        private ntype n;
        /// <summary>
        /// first item in problem
        /// </summary>
        private int fitem;
        /// <summary>
        /// items with solution variable
        /// </summary>
        private ItemE[] items;
        /// <summary>
        /// last item in problem 
        /// </summary>
        private int litem;
        /// <summary>
        /// first item considered for reduction
        /// </summary>
        private int ftouch;
        /// <summary>
        /// last item considered for reduction 
        /// </summary>
        private int ltouch;
        /// <summary>
        /// current core is [s, t]
        /// </summary>
        private int s, t;
        /// <summary>
        /// break item
        /// </summary>
        private int b;
        /// <summary>
        /// first item returned by partial sort
        /// </summary>
        private int fpart;
        /// <summary>
        /// last item returend by partial sort
        /// </summary>
        private int lpart;
        /// <summary>
        /// weight sum up to fpart
        /// </summary>
        private stype wfpart;
        /// <summary>
        /// first sorted item
        /// </summary>
        private int fsort;
        /// <summary>
        /// last sorted item
        /// </summary>
        private int lsort;
        /// <summary>
        /// weight sum up to fsort
        /// </summary>
        //private stype wfsort;
        /// <summary>
        /// current capasity
        /// </summary>
        private stype c;
        /// <summary>
        /// original capacity
        /// </summary>
        private stype cstar;
        /// <summary>
        /// current solution
        /// </summary>
        private stype z;
        /// <summary>
        /// optiaml solution
        /// </summary>
        private stype zstar;
        /// <summary>
        /// weight sum of zstar
        /// </summary>
        private stype zwsum;
        /// <summary>
        /// items for deriving bounds
        /// </summary>
        private itype ps, ws, pt, wt;

        /// <summary>
        /// current vector number
        /// </summary>
        private btype vno;
        /// <summary>
        /// current last MAXV items
        /// </summary>
        private int[] vitem = new int[Constants.MAXV];
        /// <summary>
        /// optimal set of items
        /// </summary>
        private int[] ovitem = new int[Constants.MAXV];
        /// <summary>
        /// optimal solution vector
        /// </summary>
        private btype ovect;

        /// <summary>
        /// dantzig upper bound
        /// </summary>
        private stype dantzig;
        /// <summary>
        /// global upper bound
        /// </summary>
        private stype ub;
        /// <summary>
        /// profit sum up to b
        /// </summary>
        private stype psumb;
        /// <summary>
        /// weight sum up to b
        /// </summary>
        private stype wsumb;
        /// <summary>
        /// used for restoring x
        /// </summary>
        private boolean firsttime;
        /// <summary>
        /// is x welldefined
        /// </summary>
        private boolean welldef;
        /// <summary>
        /// set of partial vectors
        /// </summary>
        private SetState d;
        //private Interval intv1, intv2;
        //private Interval intv1b, intv2b;
        private Interval[] intv;
        private int intv1;
        private int intv2;
        private int intv1b;
        private int intv2b;
        /* debug */
        long iterates;        /* counters used to obtain specific */
        long simpreduced;     /* information about the solution process */
        long pireduced;
        long pitested;
        long maxstates;
        long coresize;
        //long bzcore;

        #endregion

        #region MyRegion
        private ptype DET(Item item1, Item item2)
        {
            return (double)item1.Value * (double)item2.Weight - (double)item2.Value  * (double)item1.Weight;
        }

        private ptype DET(item i, item j)
        {
            return (double)items[i].Item.Weight * (double)items[j].Item.Value - (double)items[j].Item.Weight  * (double)items[i].Item.Value;
        }

        private ptype DET(ItemE item1, ItemE item2)
        {
            return (double)item1.Item.Value * (double)item2.Item.Weight - (double)item2.Item.Value  * (double)item1.Item.Weight;
        }
        private ptype DET(ntype a, ntype b, ntype c, ntype d)
        {
            return (double)a * (double)d - (double)b * (double)c;
        }
        private ptype DET(stype a, stype b, ntype c, ntype d)
        {
            return ((double)a * (double)d) - (double)b * (double)c;
        }
        private ptype DET(ntype a, ntype b, stype c, stype d)
        {
            return ((double)a * (double)d) - (double)b * (double)c;
        }
        private void Swap(ref ItemE item1, ref ItemE item2)
        {
            ItemE buff = item1;
            item1 = item2;
            item2 = buff;
        }

        public string GetName()
        {
            return "Minknap";
        }

        private void PrintItems()
        {
            Console.WriteLine("id\tweight\tvalue\tEff");
            for (int i = 0; i < items.Length; i++)
            {
                double eff = (double)items[i].Item.Value / (double)items[i].Item.Weight;
                Console.WriteLine(i + "\t" + items[i].Item.GetInfo() + "\t" + String.Format("{0:00.00}", eff));
            }
        }
        #endregion

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public IKnapsackResult Solve(IKnapsack knapsack)
        {
            DiscreteKnapsack discreteKnapsack = (DiscreteKnapsack)knapsack;
            InitializeMemory(discreteKnapsack);
            //PrintItems();
            PartSort(fitem, litem, 0, Constants.PARTIATE);
            //PrintItems();

            FindBreak();
            ub = dantzig;
            firsttime = 1;
            
            while (true)
            {
                iterates++;
                s = b - 1;
                t = b;
                InitFirst(psumb, wsumb);
                InitVect();
                ReduceSet();

                while ((d.size > 0) && z < ub)
                {
                    if (t <= lsort)
                    {
                        if (HasChance(t, Constants.RIGHT))
                        {
                            Multiply(t, Constants.RIGHT);
                        }

                        t++;
                        
                    }

                    ReduceSet();
                    if (s >= fsort)
                    {
                        if (HasChance(s, Constants.LEFT))
                            Multiply(s, Constants.LEFT);
                        s--;
                    }

                    ReduceSet();
                }

                DefineSolution();
                if (welldef == 1)
                    break;
            }

            List<Item> retItems = new List<Item>();
            for (int i = 0; i < n; i++)
            {
                if (items[i].x==1)
                    retItems.Add(items[i].Item);
            }

            CleanUp();
            KnapsackResult result = new KnapsackResult(retItems, zstar);
            return result;
        }

        private void CleanUp()
        {
            d.set1 = null;
            intv = null;
            items = null;
            //ovitem = null;
            //vitem = null;
        }

        private void DefineSolution()
        {
            item f, l, i;
            stype psum, wsum;
            btype j, k;

            if (firsttime > 0)
            {
                zstar = z;
                firsttime = 0;
            }

            psum = z;
            wsum = zwsum;
            f = fsort - 1;
            l = lsort + 1;

            for (j = 0; j < Constants.MAXV; j++)
            {
                k = ovect & ((btype)1 << (int)j);
                i = ovitem[j];
                //ItemE ii = ovitem[j];
                // TODO ??
                if (i>-1)
                {
                    if (items[i].x == 1)
                    {
                        if (i > f)
                            f = i;
                        if (k!=0)
                        {
                            psum += (long)items[i].Item.Value;
                            wsum += (long)items[i].Item.Weight;
                            items[i].x = 0;
                        }
                    }
                    else
                    {
                        if (i < l)
                            l = i;
                        if (k != 0)
                        {
                            psum -= (long)items[i].Item.Value;
                            wsum -= (long)items[i].Item.Weight;
                            items[i].x = 1;
                        }
                    }
                }
                
            }

            if (psum == psumb && wsum == wsumb)
                welldef = 1;
            else
                welldef = 0;

            if (welldef != 1)
            {
                fsort = f + 1;
                lsort = l - 1;
                intv1 = intv1b;
                intv2 = intv2b;
                c = wsum;
                z = psum - 1;
                ub = psum;
                maxstates = 0;
            }
        }

        private void Multiply(item h, int side)
        {
            //states
            int i, j, k, m;
            itype p, w;
            btype mask0, mask1;
            int r1, rm;
            if(d.size==0)
                return;
            if (side == Constants.RIGHT)
            {
                p = items[h].Item.Value;
                w = items[h].Item.Weight;
            }
            else
            {
                p = -items[h].Item.Value;
                w = -items[h].Item.Weight;
            }

            /* keep track on solution vector */
            vno++;
            if (vno == Constants.MAXV)
                vno = 0;
            mask1 = (btype)1 << (int)vno;
            mask0 = ~mask1;

            vitem[vno] = h;
            /* initialize limits */

            r1 = d.fset;
            rm = d.lset;
            k = 0;
            m = rm + 1;

            d.set1[k].psum = -1;
            d.set1[k].wsum = d.set1[r1].wsum + Math.Abs(p) + 1;
            d.set1[m].wsum = d.set1[rm].wsum + Math.Abs(w) + 1;

            for (i = r1, j = r1;  (i !=m) || (j!=m); )
            {
                if (d.set1[i].wsum <= d.set1[j].wsum + w)
                {
                    if (d.set1[i].psum > d.set1[k].psum)
                    {
                        if (d.set1[i].wsum > d.set1[k].wsum)
                            k++;
                        d.set1[k].psum = d.set1[i].psum;
                        d.set1[k].wsum = d.set1[i].wsum;
                        d.set1[k].vect = d.set1[i].vect & mask0;
                    }
                    i++;
                }
                else
                {
                    if (d.set1[j].psum + p > d.set1[k].psum)
                    {
                        if (d.set1[j].wsum + w > d.set1[k].wsum)
                            k++;
                        d.set1[k].psum = d.set1[j].psum + p;
                        d.set1[k].wsum = d.set1[j].wsum + w;
                        d.set1[k].vect = d.set1[j].vect | mask1;
                    }

                    j++;
                }
            }

            d.fset = 0;
            d.lset = k;
            d.size = d.lset - d.fset + 1;
            coresize++;
            if (d.size > maxstates)
                maxstates = d.size;

        }

        private bool HasChance(item i, int side)
        {
            if (d.size == 0)
                return false;

            int j, m;
            ptype p, w, r;

            stype pp, ww;

            if (side == Constants.RIGHT)
            {
                if (d.set1[d.fset].wsum <= c - items[i].Item.Weight)
                    return true;
                p = ps;
                w = ws;
                pitested++;
                pp = items[i].Item.Value - z - 1;
                ww = items[i].Item.Weight - c;
                r = -DET(pp, ww, (ntype)p, (ntype)w);
                for (j = d.fset, m = d.lset + 1; j!=m; j++)
                {
                    if (DET(d.set1[j].psum, d.set1[j].wsum, (ntype) p, (ntype) w) >= r)
                        return true;
                }
            }
            else
            {
                if (d.set1[d.lset].wsum > c + items[i].Item.Weight)
                    return true;
                p = pt;
                w = wt;
                pitested++;
                pp = -items[i].Item.Value - z - 1;
                ww = -items[i].Item.Weight - c;
                r = -DET(pp, ww, (ntype)p, (ntype)w);
                for (j = d.lset, m = d.fset - 1; j != m; j--)
                {
                    if (DET(d.set1[j].psum, d.set1[j].wsum, (ntype)p, (ntype)w) >= r)
                        return true;
                }
            }

            pireduced++;
            return false;
        }

        private void ReduceSet()
        {
            int i, m, k;
            ptype ps, ws, pt, wt, r;
            stype z, c;
            int r1, rm, v;
            item f=0, l=0;

            if (d.size == 0)
                return;

            // initialize limits
            r1 = d.fset;
            rm = d.lset;
            //// TODO ???
            
            ///
            v = FindVector(this.c, r1, rm);
            if (v == -1) // all states infeasible
            {
                
                v = r1 - 1;
                /// ??
                //return;
            }
            else
                if (d.set1[v].psum > this.z)
                    ImproveSolution(v);

            c = this.c;
            z = this.z + 1;
            k = this.d.setm;

            // expand core, and choose ps, ws
            if (s < fsort)
            {
                if (intv1 == intv1b)
                {
                    ps = Constants.PMAX;
                    ws = Constants.WMAX;
                }

                else
                {
                    Pop(Constants.LEFT, ref f, ref l);
                    if (f < this.ftouch)
                        this.ftouch = f;
                    /* default: pick first item */
                    ps = items[f].Item.Value;
                    ws = items[f].Item.Weight;
                    SimpReduced(Constants.LEFT, ref f, ref l);
                    if (f <= l)
                    {
                        PartSort(f, l, 0, Constants.SORTALL);
                        fsort = f;
                        ps = items[s].Item.Value;
                        ws = items[s].Item.Weight;
                    }
                }
            }
            else {
                ps = items[s].Item.Value;
                ws = items[s].Item.Weight;
            }

            // expand core and choose pt, wt
            if (t > lsort)
            {
                if (intv2 == intv2b)
                {
                    pt = Constants.PMIN;
                    wt = Constants.WMIN;
                }
                else
                {
                    Pop(Constants.RIGHT, ref f, ref l);
                    if (l > ltouch)
                        ltouch = l;
                    pt = items[l].Item.Value;
                    wt = items[l].Item.Weight;
                    
                    SimpReduced(Constants.RIGHT, ref f, ref l);
                    if (f <= l)
                    {
                        PartSort(f, l, 0, Constants.SORTALL);
                        lsort = l;
                        pt = items[t].Item.Value;
                        wt = items[t].Item.Weight;
                    }
                }
            }
            else
            {
                pt = items[t].Item.Value;
                wt = items[t].Item.Weight;
            }

            /* now do the reduction */
            r = DET(z, c, (ntype)ps, (ntype)ws);
            for (i = rm, m = v; i != m; i--)
            {
                if (DET(d.set1[i].psum, d.set1[i].wsum, (ntype) ps, (ntype) ws)>=r)
                {
                    k--;
                    d.set1[k] = d.set1[i];
                }
            }

            r = DET(z, c, (ntype)pt, (ntype)wt);
            for (i = v, m = r1 - 1; i != m; i--)
            {
                if (DET(d.set1[i].psum, d.set1[i].wsum, (ntype) pt, (ntype) wt) >= r)
                {
                    k--;
                    d.set1[k] = d.set1[i];
                }
            }

            this.ps = (ntype)ps;
            this.ws = (ntype) ws;
            this.pt = (ntype) pt;
            this.wt = (ntype) wt;
            d.fset = k;
            d.lset = d.setm - 1;
            d.size = d.lset - d.fset + 1;
        }

        private void SimpReduced(int side, ref int f, ref int l)
        {
            item i, j, k;
            ntype pb, wb;
            ptype q, r;
            int redu;

            if(d.size == 0)
            {
                f = l + 1;
                return;
            }
            if (l < f)
                return;
            pb = items[b].Item.Value;
            wb = items[b].Item.Weight;

            q = DET(z + 1 - psumb, c - wsumb, (ntype)pb, (ntype)wb);
            r = -DET(z + 1 - psumb, c - wsumb, (ntype)pb, (ntype)wb);

            i = f;
            j = l;

            redu = 0;
            if(side == Constants.LEFT)
            {
                k = fsort - 1;
                while (i <= j)
                {
                    if(DET(items[j].Item.Value, items[j].Item.Weight, (ntype)pb, (ntype)wb) > r)
                    {
                        // not feasible
                        Swap(ref items[i], ref items[j]);
                        i++;
                        redu++;
                    }
                    else
                    {
                        // feasible
                        Swap(ref items[j], ref items[k]);
                        j--;
                        k--;
                    }
                }
                l = fsort - 1;
                f = k + 1;
            }
            else
            {
                k = lsort + 1;
                while (i <= j)
                {
                    if(DET(items[i].Item.Value, items[i].Item.Weight, pb, wb) < q)
                    {
                        Swap(ref items[i], ref items[j]);
                        j--;
                        redu++;
                    }
                    else
                    {
                        Swap(ref items[i], ref items[k]);
                        i++;
                        k++;
                    }
                }
                f = this.lsort + 1;
                l = k - 1;
            }
            simpreduced += redu;
        }

        private void ImproveSolution(int v)
        {
            if (d.set1[v].wsum > c)
                Console.WriteLine("wrong improve solution");
            if (d.set1[v].psum <= z)
                Console.WriteLine("not improved solution");
            z = d.set1[v].psum;
            zwsum = d.set1[v].wsum;
            ovect = d.set1[v].vect; 

            Array.Copy(vitem, ovitem, Constants.MAXV);
        }

        private int FindVector(Int64 ws, int f, int l)
        {
            int m;
            if (f > l)
                Console.WriteLine("findvect: empty set");
            if (d.set1[f].wsum > ws)
                return -1;
            if (d.set1[l].wsum <= ws)
                return l;

            while(l - f > Constants.SYNC)
            {
                m = f + (l - f) / 2;
                if (d.set1[m].wsum > ws)
                    l = m - 1;
                else
                    f = m;
            }
            while (d.set1[l].wsum > ws) l--;
            return l;

        }

        private void InitVect()
        {
             //TODO ?????
            for (int i = 0; i < Constants.MAXV; i++)
                vitem[i]=-1;
            vno = Constants.MAXV-1;
        }

        private void InitFirst(stype ps, stype ws)
        {
            int k;
            d.size = 1;
            d.set1 = new State[Constants.MAXSTATES];
            d.setm = Constants.MAXSTATES - 1;
            d.fset = 0;
            d.lset = 0;

            k = d.fset;
            d.set1[k].psum = ps;
            d.set1[k].wsum = ws;
            d.set1[k].vect = 0;
        }

        private void FindBreak()
        {
            item i, m;
            stype psum, wsum, c, r;

            psum = 0;
            wsum = 0;
            c = cstar;
            for(i = fitem; wsum <= c; i++)
            {
                items[i].x = 1;
                psum += items[i].Item.Value;
                wsum += items[i].Item.Weight;
            }
            i--;
            psum -= items[i].Item.Value;
            wsum -= items[i].Item.Weight;

            fsort = fpart;
            lsort = lpart;
            ftouch = fpart;
            ltouch = lpart;
            b = i;
            psumb = psum;
            wsumb = wsum;
            dantzig = psum + ((c - wsum) * items[i].Item.Value) / items[i].Item.Weight;

            // find greedy solution
            r = c - wsum;
            for(i = b, m = litem; i <= m; i++)
            {
                items[i].x = 0;
                if(items[i].Item.Weight <= r)
                {
                    psum += items[i].Item.Value;
                    r -= items[i].Item.Weight;
                }
            }

            z = psum - 1;
            zstar = 0;
            this.c = cstar;
        }

        #region PartSort
        private void PartSort(int f, int l, stype ws, int what)
        {
            stype mp, mw;
            item i, j, m=0;
            Item mm;
            stype wi;
            item d = l - f + 1;
            if (d < 1)
                Console.WriteLine("negative interval in partsort");
            if (d > Constants.MINMED)
                mm = Median(f, l, (ntype)Math.Sqrt(d));
            else
            {
                if (d > 1)
                {
                    m = f + d / 2;
                    if (DET(items[f], items[m]) < 0)
                        Swap(ref items[f], ref items[m]);
                    if (d > 2)
                        if (DET(items[m], items[l]) < 0)
                        {
                            Swap(ref items[m], ref items[l]);
                            if (DET(items[f], items[m]) < 0)
                                Swap(ref items[f], ref items[m]);
                        }
                }

                mm = items[m].Item;
            }
            
            if (d > 3)
            {
                mp = mm.Value;
                mw = mm.Weight;
                i = f;
                j = l;
                wi = ws;
                while (true)
                {
                    do
                    {
                        wi += items[i].Item.Weight;
                        i++;
                    } while (DET(items[i].Item.Value, items[i].Item.Weight, mp, mw) > 0);
                    do
                    {
                        j--;
                    } while (DET(items[j].Item.Value, items[j].Item.Weight, mp, mw) < 0);

                    if (i > j)
                        break;

                    Swap(ref items[i], ref items[j]);
                    //Console.WriteLine("swap\t" + items[i].Item.GetInfo() + "\t" + items[j].Item.GetInfo());
                }
                //Console.WriteLine("End\n");

                if (wi <= cstar)
                {
                    if(what==Constants.SORTALL)
                        PartSort(f, i-1, ws, what);
                    if (what == Constants.PARTIATE)
                        Push(Constants.LEFT, f, i - 1);
                    PartSort(i, l, wi, what);
                }
                else
                {
                    if (what == Constants.SORTALL)
                        PartSort(i, l, wi, what);
                    if (what == Constants.PARTIATE)
                        Push(Constants.RIGHT, i, l);
                    PartSort(f, i-1, ws, what);
                }
            }

            if ((d <= 3) || (what == Constants.SORTALL))
            {
                fpart = f;
                lpart = l;
                wfpart = ws;
            }
        }

        /// <summary>
        /// Find median r of items [f1, f1+s, f1+2s, ... l1],
        /// and ensure the ordering f1 >= r >= l1.
        /// </summary>
        private Item Median(item f1, item l1, ntype s)
        {
            ptype mp, mw;
            item i, j;
            item f, l, k, m, q;
            ntype n, d;

            n = ((ntype)(l1 - f1)) / s;         /* number of values      */
            f = f1;                             /* calculated first item */
            l = f1 + s * n;                     /* calculated last item  */
            k = l;                              /* saved last item       */
            q = f + s * (n / 2);                /* middle value          */
            Item current = new Item(0, 0);

            while (true)
            {
                d = (l - f + s) / s;
                m = f + s * (d / 2);
                if (d > 1)
                {
                    if (DET(items[f], items[m]) < 0)
                        Swap(ref items[f], ref items[m]);
                    if (d > 2)
                        if (DET(items[m], items[l]) < 0)
                        {
                            Swap(ref items[m], ref items[l]);
                            if (DET(items[f], items[m]) < 0)
                                Swap(ref items[f], ref items[m]);
                        }
                }

                if (d <= 3)
                {
                    current = items[q].Item;
                    break;
                }

                //current = items[m];
                mp = items[m].Item.Value;
                mw = items[m].Item.Weight;
                current.Weight = items[m].Item.Weight;
                current.Value = items[m].Item.Value;
                //r = m;
                i = f;
                j = l;
                while (true)
                {
                    do
                    {
                        i += (item)s;
                    } while (DET(items[i].Item.Value, items[i].Item.Weight, (ntype)mp,(ntype) mw) > 0);

                    do
                    {
                        j -= (item)s;
                    } while (DET(items[j].Item.Value, items[j].Item.Weight, (ntype)mp, (ntype)mw) < 0);

                    if (i > j)
                        break;
                    Swap(ref items[i], ref items[j]);
                }

                if ((j <= q) && (q <= i))
                    break;
                if (i > q)
                    l = j;
                else
                    f = i;
            }
            Swap(ref items[k], ref items[l1]);
            return current;
        }

        #endregion


        #region push/pop intervals part sort
        private void Push(int side, item f, item l)
        {
            int pos = 0;

            switch (side)
            {
                case Constants.LEFT:
                    pos = intv1++;
                    break;
                case Constants.RIGHT:
                    pos = intv2--;
                    break;
            }
            if (intv1 == intv2)
                Console.WriteLine("interval stack full");
            intv[pos].f = f;
            intv[pos].l = l;
        }

        private void Pop(int side, ref item f, ref item l)
        {
            int pos = 0;
            switch (side)
            {
                case Constants.LEFT:
                    if (intv1 == intv1b)
                        Console.WriteLine("Err pop left");

                    pos=--intv1;
                    break;
                case Constants.RIGHT:
                    if (intv2 == intv2b)
                        Console.WriteLine("Err pop right");
                    pos = ++intv2;
                    break;
            }
            f = intv[pos].f;
            l = intv[pos].l;
        }
        #endregion

        #region Initialize Memory
        private void InitializeMemory(DiscreteKnapsack discreteKnapsack)
        {
            n = (ntype)discreteKnapsack.Items.Length;
            c = 0;

            cstar = discreteKnapsack.Capacity;
            intv = new Interval[Constants.SORTSTACK];
            intv1 = intv1b = 0;
            intv2 = intv2b = Constants.SORTSTACK - 1;
            fitem = 0;
            litem = discreteKnapsack.Items.Length - 1;
            iterates = 0;
            simpreduced = 0;
            pireduced = 0;
            pitested = 0;
            maxstates = 0;
            maxstates = 0;
            coresize = 0;

            fsort = litem;
            lsort = fitem;
            items = new ItemE[n];
            for (int i = 0; i < n; i++)
            {
                items[i].Item = discreteKnapsack.Items[i];
                items[i].x = 0;
            }
        }

        public void SetCustomParameters(object[] objects)
        {

        }
        #endregion
    }
}
