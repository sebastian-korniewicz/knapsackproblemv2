﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.algorithms
{
    public class DynamicProgrammingOpt : IKnapsackSolver
    {
        public string GetName()
        {
            return "Dynamic programming with memory optimization";
        }

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public IKnapsackResult Solve(IKnapsack knapsack)
        {
            DiscreteKnapsack discreteKnapsack = (DiscreteKnapsack) knapsack;
            int[,] sumOfValues = new int[2, discreteKnapsack.Capacity + 1];
            
            for (int k = 0; k <= (int)discreteKnapsack.Capacity; k++)
                sumOfValues[0, k] = 0;
            sumOfValues[0, 1] = 0;

            for (int i = 1; i <= discreteKnapsack.Items.Length; i++)
            {
                for (int j = 1; j <= (int)discreteKnapsack.Capacity; j++)
                {
                    if (discreteKnapsack.Items[i-1].Weight > j)
                    {
                        sumOfValues[i % 2, j] = sumOfValues[(i - 1) % 2, j];
                    }
                    else
                    {
                        sumOfValues[i % 2, j] = Math.Max(
                            sumOfValues[(i - 1) % 2, j],
                            sumOfValues[(i - 1) % 2, j - discreteKnapsack.Items[i-1].Weight] + discreteKnapsack.Items[i-1].Value);
                    }
                }
            }
            
            KnapsackResult knapsackResult = new KnapsackResult(
                null,
                (long)sumOfValues[discreteKnapsack.Items.Length % 2, discreteKnapsack.Capacity]);
            return knapsackResult;
        }

        public void SetCustomParameters(object[] objects)
        {

        }
    }
}
