﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.algorithms
{
    public class TestSolver : IKnapsackSolver
    {
        private int A = 0;
        private double B = 0;

        public List<string> GetCustomParameters()
        {
            return new List<string> {A.ToString(), B.ToString()};
        }

        public List<string> GetCustomParametersNames()
        {
            return new List<string> { "A", "B"};
        }

        public string GetName()
        {
            return "Test Solver";
        }

        public void SetCustomParameters(object[] objects)
        {
            A = (int) objects[0];
            B = (double) objects[1];
        }

        public IKnapsackResult Solve(IKnapsack discreteKnapsack)
        {
            return null;
        }
    }
}
