﻿using KnapsackProblem.algorithms.sorting;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.algorithms.heuristic
{
    public class TreeMutationHeuristic : IKnapsackSolver
    {
        public string GetName()
        {
            throw new NotImplementedException();
        }
        Random random;

        public IKnapsackResult Solve(IKnapsack knapsack)
        {
            DiscreteKnapsack discreteKnapsack = (DiscreteKnapsack)knapsack;
            random = new Random();
            // liczba przedmiotow
            int n = discreteKnapsack.Items.Length;
            Comparer<Item> orderByWeight = Comparer<Item>.Create(
                    (a, b) => a.Weight.CompareTo(b.Weight)
                );
            //Console.WriteLine(discreteKnapsack.GetInfo());
            //sortowanie
            MergeSorter mergeSorter = new MergeSorter();
            // items zawiera przedmioty posortowane niemalejaco wg wagi
            Item[] items = mergeSorter.IterativeMergeSort(discreteKnapsack.Items, orderByWeight);

            // ilosc najmniejszych przedmiotow zmieszczonych do plecaka 
            int k = calcK(items, discreteKnapsack.Capacity);
            // ilosc najwiekszych przedmiotow zmieszczonych do plecaka
            int t = calcT(items, discreteKnapsack.Capacity);

            int level = random.Next(t, k);
            var gVector = G(n, level);
            var limitVector = calcLimit(items, discreteKnapsack.Capacity, gVector, level);
            var randVector = randVertex(limitVector, level);
            long sumOfWeight = 0;
            long sumOfValue = 0;

            for (int i = 0; i < randVector.Length; i++)
                if (randVector[i])
                {
                    sumOfWeight += items[i].Weight;
                    sumOfValue += items[i].Value;
                }

            int biggestItem = discreteKnapsack.Items.Length;

            while (sumOfWeight>discreteKnapsack.Capacity)
            {
                do { } while (!randVector[--biggestItem]);
                sumOfWeight -= items[biggestItem].Weight;
                sumOfValue -= items[biggestItem].Value;
                randVector[biggestItem] = false;
            }

            List<Item> retList = new List<Item>();
            for (int i = 0; i < randVector.Length; i++)
                if (randVector[i])
                    retList.Add(items[i]);

            KnapsackResult knapsackResult = new KnapsackResult(retList, sumOfValue);
            return knapsackResult;

                    //print(limitVector);
                    //Dictionary<String, int> dictionary = new Dictionary<String, int>();
                    //int nn = 10000000;
                    //for (int i = 0; i < nn; i++)
                    //{
                    //    var y = randVertex(limitVector, level);
                    //    String yx = "";
                    //    for (int j = 0; j < y.Length; j++)
                    //        yx += y[j] ? "1" : "0";

                    //    if (dictionary.ContainsKey(yx))
                    //    {
                    //        int currentCount;
                    //        dictionary.TryGetValue(yx, out currentCount);
                    //        dictionary[yx] = currentCount + 1;
                    //    }
                    //    else
                    //    {
                    //        dictionary.Add(yx, 1);
                    //    }
                    //}

                    //var list = dictionary.Keys.ToList();
                    //var values = dictionary.Values.ToList();
                    //long sumOfVal = 0;
                    //for (int i = 0; i < values.Count; i++)
                    //{
                    //    sumOfVal += values[i];
                    //}
                    //list.Sort();
                    //Console.WriteLine("");
                    //foreach (var key in list)
                    //{
                    //    Console.WriteLine("{0}: {1}%", key, (double)dictionary[key]/(double)sumOfVal*100);
                    //}


                    return null;
        }

        private bool[] randVertex(bool[] limitVector, int level)
        {
            bool[] randVector = new bool[limitVector.Length];
            int iterRight = 0;
            int iterLeft = 0;

            int firstRand = iterRight;
            int amount = 0;

            do { } while (!limitVector[iterRight++]);
            iterRight--;
            int iterLimitVector = iterRight;
            do
            {

                firstRand = random.Next(iterLeft, iterRight + 1);
                randVector[firstRand] = true;

                amount++;
                if (firstRand == iterLimitVector)
                {
                    if (level > amount)
                    {
                        iterLeft = firstRand + 1;
                        do { } while (!limitVector[++iterLimitVector]);
                        iterRight = iterLimitVector;
                    }
                } else
                {
                    iterLimitVector = -1;
                    iterLeft = firstRand + 1;
                    iterRight = limitVector.Length - (level - amount);
                }
            } while (level > amount);
            return randVector;
        }

        private bool[] calcLimit(Item[] items, long capacity, bool[] gVector, int level)
        {
            bool[] limit = new bool[gVector.Length];
            bool[] buffor = new bool[gVector.Length];
            Array.Copy(gVector, buffor, gVector.Length);

            long sumOfWeight = 0;
            for (int i = 0; i < level; i++)
                sumOfWeight += items[items.Length-i-1].Weight;
            int iter2 = items.Length - 1;
            
            while (sumOfWeight>capacity && iter2>=level)
            {
                sumOfWeight -= items[iter2].Weight;
                sumOfWeight += items[iter2-level].Weight;
                buffor[iter2] = false;
                buffor[iter2 - level] = true;
                iter2--;
            }

            iter2++;
            int iter1 = iter2 - level+1;
            Array.Copy(buffor, limit, gVector.Length);

            while ((iter2 < items.Length || (sumOfWeight < capacity || iter1 + 1 != iter2)) && iter2 < items.Length )
            {
                if (sumOfWeight <= capacity)
                {
                    buffor[iter2] = true;
                    buffor[iter1] = false;
                    sumOfWeight += items[iter2].Weight;
                    sumOfWeight -= items[iter1].Weight;
                    iter2++;
                    iter1++;
                }
                else
                {
                    buffor[iter1-1] = true;
                    buffor[iter1] = false;
                    sumOfWeight += items[iter1 -1].Weight;
                    sumOfWeight -= items[iter1].Weight;
                    iter1++;
                }

                if (sumOfWeight<=capacity)
                {
                    Array.Copy(buffor, limit, gVector.Length);
                } 
            }
            return limit;
        }

        private int calcK(Item [] items, long capacity)
        {
            int sumOfWeight = 0;
            int i = -1;
            while ((i<items.Length) && (sumOfWeight <= capacity))
            {
                sumOfWeight += items[++i].Weight;
            }

            if (sumOfWeight > capacity)
            {
                i--;
            }
            return i+1;
        }

        private int calcT(Item[] items, long capacity)
        {
            int sumOfWeight = 0;
            int i = items.Length;
            while ((i >= 1) && (sumOfWeight <= capacity))
            {
                sumOfWeight += items[--i].Weight;
            }

            if (sumOfWeight > capacity)
            {
                i++;
            }
            return items.Length - i;
        }

        private bool[] G(int n, int k)
        {
            bool[] x = new bool[n];
            for (int i = 0; i < k; i++)
                x[n-i-1] = true;
            return x;
        }

        private void print(bool[] x)
        {
            for (int j = 0; j < x.Length; j++)
                if (x[j])
                    Console.Write("1");
                else
                    Console.Write("0");
            Console.WriteLine("");
        }

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public void SetCustomParameters(object[] objects)
        {
            
        }
    }
}
