﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.algorithms.Combo
{
    using boolean = System.Int32;
    using ntype = System.Int32;            /* number of states/items   */
    using itype = System.Int32;             /* item profits and weights */
    using stype = System.Int64;             /* sum of pofit or weight   */
    using ptype = System.Double;            /* product type (sufficient precision) */
    using btype = System.UInt64;            /* binary representation of solution */
    using item = System.Int32;
    using prod = System.Double;
    static class Constants
    {
        /// <summary>
        /// when to switch to linear scan in bins
        /// </summary>
        public const int SYNC = 5;
        /// <summary>
        /// parameter M1 from paper: 1000 normally
        /// </summary>
        public const int MINRUDI = 1000;
        /// <summary>
        /// parameter M2 from paper: 2000 normally
        /// </summary>
        public const int MINSET = 2000;
        /// <summary>
        /// parameter M3 from paper: 10000 normally
        /// </summary>
        public const int MINHEUR = 10000;
        public const int MAXSTATES = 1500000;

        /// <summary>
        /// depth of stack used in qsort
        /// </summary>
        public const int SORTSTACK = 100;

        /// <summary>
        /// find exact median in qsort if larger size
        /// </summary>
        public const int MINMED = 1000;



        public const int TRUE = 1;
        public const int FALSE = 0;

        public const int LEFT = 1;
        public const int RIGHT = 2;

        public const int PARTIATE = 1;
        public const int SORTALL = 2;

        /// <summary>
        /// number of bits in a long integer
        /// </summary>
        public const int MAXV = sizeof(btype) * 8;

        /// <summary>
        /// profit of worlds most efficient item
        /// </summary>
        public const int PMAX = 1;

        /// <summary>
        /// weight of worlds most efficient item
        /// </summary>
        public const int WMAX = 0;

        /// <summary>
        /// profit of worlds least efficient item
        /// </summary>
        public const int PMIN = 0;

        /// <summary>
        /// weight of worlds least efficient item
        /// </summary>
        public const int WMIN = 1;
    }

    struct Interval
    {
        /// <summary>
        /// first item in interval
        /// </summary>
        public int f;
        /// <summary>
        /// last item in interval
        /// </summary>
        public int l;
    }

    struct State
    {
        /// <summary>
        /// profit sum
        /// </summary>
        public stype psum;
        /// <summary>
        /// weight sum
        /// </summary>
        public stype wsum;
        /// <summary>
        /// solution vector
        /// </summary>
        public btype vect;
    }

    /// <summary>
    /// set of states
    /// </summary>
    struct PartSet
    {
        /// <summary>
        /// set size
        /// </summary>
        public ntype size;
        /// <summary>
        /// first element in set
        /// </summary>
        public State[] set1;
        /// <summary>
        /// first element in array
        /// </summary>
        public int fset;
        /// <summary>
        /// last element in set
        /// </summary>
        public int lset;
        /// <summary>
        /// last element in array
        /// </summary>
        public int setm;

        /// <summary>
        /// current vector number
        /// </summary>
        public btype vno;
        /// <summary>
        /// current last MAXVa.items
        /// </summary>
        /// ??
        public int[] vitem;
        /// <summary>
        /// optimal set ofa.items 
        /// </summary>
        /// ??
        public int[] ovitem;
        /// <summary>
        /// optimal solution vector
        /// </summary>
        public btype ovect;
    }

    struct ItemE
    {
        public Item Item;
        /// <summary>
        /// solution variable
        /// </summary>
        public boolean x;
    }

    struct AllInfo
    {
        /// <summary>
        /// number ofa.items
        /// </summary>
        public ntype n;

        /// <summary>
        /// first item in problem
        /// </summary>
        public int fitem;

        

        /// <summary>
        /// last item in problem 
        /// </summary>
        public int litem;

        /// <summary>
        /// current core is [s, t]
        /// </summary>
        public int s, t;

        /// <summary>
        /// break item
        /// </summary>
        public int b;

        /// <summary>
        /// first item returned by partial sort
        /// </summary>
        public int fpart;

        /// <summary>
        /// last item returend by partial sort
        /// </summary>
        public int lpart;

        /// <summary>
        /// weight sum up to fpart
        /// </summary>
        public stype wfpart;

        /// <summary>
        /// first sorted item
        /// </summary>
        public int fsort;

        /// <summary>
        /// last sorted item
        /// </summary>
        public int lsort;

        /// <summary>
        /// weight sum up to fsort
        /// </summary>
        public stype wfsort;

        /// <summary>
        /// capasity
        /// </summary>
        public stype c;

        /// <summary>
        /// incumbent solution
        /// </summary>
        public stype z;

        /// <summary>
        /// weight sum of incumbent solution
        /// </summary>
        public stype zwsum;


        // TODO info
        public stype lb;

        public boolean fullsol;       /* which representation of solution */
        /// <summary>
        /// first item in opt solution
        /// </summary>
        public item fsol;
        /// <summary>
        /// last item in opt solution
        /// </summary>
        public item lsol;
        /// <summary>
        /// start of item array
        /// </summary>
        public ItemE[] ffull;
        /// <summary>
        /// end of item array
        /// </summary>
        public item lfull;

        public PartSet d;
        

        public itype ps, ws, pt, wt;

        /// <summary>
        /// dantzig upper bound
        /// </summary>
        public stype dantzig;

        /// <summary>
        /// global upper bound
        /// </summary>
        public stype ub;

        /// <summary>
        /// profit sum up to b
        /// </summary>
        public stype psumb;

        /// <summary>
        /// weight sum up to b
        /// </summary>
        public stype wsumb;
        public int intv1;
        public int intv2;
        public int intv1b;
        public Interval[] intv;

        public int intv2b;
        public boolean relx;
        public boolean master;
        public int coresize;
        /* debug */
        long iterates; /* counters used to obtain specific */
        long simpreduced; /* information about the solution process */
        long pireduced;
        long pitested;
        //long coresize;
        long bzcore;
        public ItemE[] items;
    }
    public class Combo : IKnapsackSolver
    {
        
        int simpreduced;
        long maxstates;
        
        private int coresize = 0;
        private int pitested;
        private int pireduced;
        private int dynheur;
        private int relaxations = 0;
        private long optsur;
        private int relfeasible;
        private int iterates;

        public string GetName()
        {
            return "COMBO";
        }

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public IKnapsackResult Solve(IKnapsack knapsack)
        {
            DiscreteKnapsack discreteKnapsack = (DiscreteKnapsack)knapsack;
            int n = discreteKnapsack.Items.Length;
            ItemE[] items = new ItemE[discreteKnapsack.Items.Length];
            for (int i = 0; i < n; i++)
                items[i].Item = discreteKnapsack.Items[i];

            stype x = ComboSolve(ref items, 0, discreteKnapsack.Items.Length - 1, discreteKnapsack.Capacity, 0, 0, 1, 0);
            List<Item> retItems = new List<Item>();
            for (int i = 0; i < n; i++)
            {
                if (items[i].x == 1)
                    retItems.Add(items[i].Item);
            }
            items = null;
            KnapsackResult result = new KnapsackResult(retItems, x);
            return result;
        }

        private stype ComboSolve(ref ItemE[] itemsTab, item f, item l, stype c, stype lb, stype ub, boolean def, boolean relx)
        {
            AllInfo a = new AllInfo();
            a.items = itemsTab;
            Interval[] inttab;
            boolean heur, rudi;


            if ((ub != 0) && (lb == ub))
                return lb;

            heur = 0;
            rudi = 0;
            a.intv = inttab = new Interval[Constants.SORTSTACK];
            a.intv1b = 0;
            a.intv2b = Constants.SORTSTACK - 1;
            a.intv1 = 0;
            a.intv2 = a.intv2b;
            a.fitem = f;
            a.litem = l;
            a.c = c;
            a.z = lb;
            a.lb = lb;
            a.relx = relx;
            a.master = ((def!=0) && (0==relx)) ? 1 : 0;
            a.coresize = 0;
            a.d.vitem = new int[Constants.MAXV];
            a.d.ovitem = new int[Constants.MAXV];

            //printItems(a);
            PartSort(ref a, a.fitem, a.litem, 0, a.c, Constants.PARTIATE);
            //printItems(a);
            FindBreak(ref a);
            a.ub = (ub==0 ? a.dantzig : ub);

            /* find and enumerate core */
            FindCore(ref a);
            ReduceSet(ref a);
            
            while((a.d.size > 0) && (a.z < a.ub))
            {
                if(a.t <= a.lsort)
                {
                    if (HasChance(ref a, a.t, Constants.RIGHT))
                        Multiply(ref a, a.t, Constants.RIGHT);
                    a.t++;
                }
                ReduceSet(ref a);

                if(a.s >= a.fsort)
                {
                    if (HasChance(ref a, a.s, Constants.LEFT))
                        Multiply(ref a, a.s, Constants.LEFT);
                    a.s--;
                }
                ReduceSet(ref a);

                // find better lower bound when needed
                if((heur==0)&&(a.d.size>Constants.MINHEUR))
                {
                    Heuristic(ref a);
                    heur = 1;
                }

                // find tight bound when needed
                if((relx==0)&&(a.d.size > Constants.MINSET))
                {
                    Surrelax(ref a);
                    relx = 1;

                }


                if ((rudi == 0) && (a.d.size > Constants.MINRUDI))
                {
                    Rudidiv(ref a);
                    rudi = 1;
                }
            }
            a.d.set1 = null;
            inttab = null;

            if ((def == 1) && (a.z > a.lb))
                DefineSolution(ref a);

            CleanUp(ref a);
            return a.z;
        }

        private void Surrelax(ref AllInfo a)
        {
            item i, j, m, f, l, b;
            ItemE[] fTable;
            ntype n, card1, card2, b1;
            stype u, minsur, maxsur, wsum;
            itype minw, maxp, maxw;
            long t1, t2;

            relaxations++;
            n = a.litem - a.fitem + 1;
            fTable = new ItemE[n];
            f = 0;
            l = n - 1;

            minw =a.items[a.fitem].Item.Weight;
            maxp = maxw = 0;
            wsum = 0;

            // copy table
            for(j = f, i = a.fitem, m=l+1; j!=m; i++, j++)
            {
                fTable[j] =a.items[i];
                wsum +=a.items[i].Item.Weight;
                if (a.items[i].Item.Weight < minw)
                    minw =a.items[i].Item.Weight;
                if (a.items[i].Item.Weight > maxw)
                    maxw =a.items[i].Item.Weight;
                if (a.items[i].Item.Value > maxp)
                    maxp =a.items[i].Item.Value;
            }

            /* find cardinality */
            b = a.b;
            b1 = b- a.fitem;
            card1 = minWeight(ref fTable, f, l, a.c) - f;       /* sum_{j=1}^{n} x_{j} \leq card1 */
            card2 = maxProfits(ref fTable, f, l, a.z) - f + 1;  /* sum_{j=1}^{n} x_{j} \geq card2 */

            /* delimiters on sur.multipliers */
            maxsur = maxw;  /* should ideally be: maxp*maxp, but may cause overflow */
            minsur = -maxw; /* should ideally be: -maxp*maxw, but may cause overflow */

            /* choose strategy */
            u = 0;
            for (; ; )
            {
                if (card2 == b1 + 1)
                {
                    solveSur(ref a, ref fTable, f, l, minsur, 0, b1 + 1, ref u); /* min card constr */
                    if (u < a.z)
                        u = a.z;
                    break;
                }
                if (card1 == b1)
                {
                    solveSur(ref a, ref fTable, f, l, 0, maxsur, b1, ref u); /* max card constr */
                    break;
                }
                if (card1 == b1 + 1)        /* dichothomy: card <= b1 or card >= b1+1 */
                {
                    solveSur(ref a, ref fTable, f, l, minsur, 0, b1 + 1, ref u); 
                    solveSur(ref a, ref fTable, f, l, 0, maxsur, b1, ref u);
                    break;
                }
                if (card2 == b1)        /* dichothomy: card <= b1 or card >= b1+1 */
                {
                    solveSur(ref a, ref fTable, f, l, 0, maxsur, b1, ref u); 
                    solveSur(ref a, ref fTable, f, l, minsur, 0, b1+1, ref u);
                    break;
                }
                u = a.dantzig;
                break;
            }
            if (u < a.ub)
                a.ub = u;
            fTable = null;
        }

        private void solveSur(ref AllInfo a, ref ItemE[] tab, int f, int l, long minsur, long maxsur, int card, ref long ub)
        {
            item i, k, m;
            stype ps, csur;
            ntype no;
            stype sur=0, u=0, z1;
            boolean feasible;

            /* find optimal surrogate multiplier, and update ub */
            surBin(ref tab, f, l, minsur, maxsur, a.c, a.dantzig, card, ref sur, ref u);
            optsur = sur;

            /* if bound <= current solution return */
            if ((u <= a.z) || (sur == 0))
            {
                if (u > ub)
                    ub = u;
                return;
            }

            /* add sur to weights and removea.items with negative weight */
            csur = a.c + sur * (long)card;
            ps = 0;
            for (i = f, k = f, m = l + 1; i != m; i++)
            {
                tab[i].Item.Weight += (itype)sur;
                tab[i].x = 0;
                if (tab[i].Item.Weight <= 0)
                {
                    ps += tab[i].Item.Value;
                    csur -= tab[i].Item.Weight;
                    tab[i].x = 1;
                    Swap(ref tab[i], ref tab[k]);
                    k++;
                }
            }

            /* solve problem to optimality */
            z1 = ps + ComboSolve(ref tab, k, l, csur, a.z - ps, 0, 1, 1);

            /* subtract weight and check cardinality */
            for (i = f, m = l + 1, no = 0; i != m; i++)
            {
                tab[i].Item.Weight -= (itype)sur;
                if (tab[i].x!=1)
                    no++;
            }

            /* if feasible cardinality and improved, save solution in extra array */
                
            if (no == card)
                relfeasible++;
            if ((z1 > a.z) && (no == card))
            {
                for (i = f, k = 0, m = l + 1; i != m; i++, k++)
                    a.ffull[k] = tab[i];
                a.z = z1;
                a.fullsol = 1;
            }

            /* output: maintain global upper bound */
            if (z1 > ub)
                ub = z1;
        }

        private void surBin(ref ItemE[] tab, int f, int l, long s1, long s2, long c, long dantzig, ntype card, ref long sur, ref long u)
        {
            item b = -1;
            stype csur, r=0, psum=0, s, d, suropt;
            itype weight, value;
            double ua, ub, gr, e, uopt;
            /* iterate surr. multiplier */
            uopt = dantzig;
            suropt = 0;

            for (; s1 <= s2;)
            {
                s = (s2 + s1) / 2;
                csur = c + s * (long)card;
                if (csur < 0)
                    csur = 0;

                sursort(ref tab, f, l, s, csur, ref psum, ref r, ref b);

                /* derive bound and gradient */
                e = 1;

                if (b == -1)
                {
                    value = 0;
                    weight = 1;
                    d = 50000000;
                }
                else
                {
                    value = tab[b].Item.Value;
                    weight = tab[b].Item.Weight;
                    d = b - f;
                }


                ua = psum + r * (prod)value / (weight + s);
                ub = psum + (r + (card - d) * e) * (prod)value / (weight + s + e);
                gr = (ub - ua) / e;

                if (ua < uopt)
                {
                    suropt = s;
                    uopt = ua;
                }
                if (gr > 0)
                    s2 = s - 1;
                else
                    s1 = s + 1;
            }
            sur = suropt;
            u = (long)uopt;
        }

        private void sursort(ref ItemE[] tab, int f, int l, stype sur, long c, ref long p1, ref long w1, ref item b)
        {
            itype s;
            prod mp, mw;
            item i, j, m=0;
            stype ws, ps;
            ItemE nn = new ItemE();
            nn.Item = new Item(0, 0);
            item l1;
            stype psum;
            int d;

            psum = 0; s = (itype)sur; l1 = l + 1;
            for (; ; )
            {
                d = l - f + 1;
                if (d > 1)
                {
                    m = f + d / 2;
                    if (DET(tab[f].Item.Value, tab[f].Item.Weight + s, tab[m].Item.Value, tab[m].Item.Weight+s) < 0)
                        Swap(ref tab[f], ref tab[m]);
                    if (d > 2)
                        if (DET(tab[m].Item.Value, tab[m].Item.Weight + s, tab[l].Item.Value, tab[l].Item.Weight+s) < 0)
                        {
                            Swap(ref tab[m], ref tab[l]);

                            if (DET(tab[f].Item.Value, tab[f].Item.Weight + s, tab[m].Item.Value, tab[m].Item.Weight+s) < 0)
                                Swap(ref tab[f], ref tab[m]);
                        }

                }

                if (d <= 3)
                    break;
                

                mp = tab[m].Item.Value;
                mw = tab[m].Item.Weight + s;
                i = f;
                j = l;
                ws = ps = 0;

                while(true)
                {
                    do
                    {
                        ws += tab[i].Item.Weight+s;
                        ps += tab[i].Item.Value;
                        i++;
                    } while (DET(tab[i].Item.Value, tab[i].Item.Weight+s, mp, mw) > 0);

                    do
                    {
                        j--;
                    } while (DET(tab[j].Item.Value, tab[j].Item.Weight + s, mp, mw) < 0);
                    if (i > j)
                        break;

                    Swap(ref tab[i], ref tab[j]);
                }
                if(ws <= c)
                {
                    f = i;
                    c -= ws;
                    psum += ps;
                }
                else
                    l = i - 1;
            }
            for (; f != l1; f++)
            {
                if(tab[f].Item.Weight+s > c)
                {
                    p1 = psum;
                    w1 = c;
                    b = f;
                    return;
                }
                c -= tab[f].Item.Weight + s;
                psum += tab[f].Item.Value;
            }
            //nn.Item.Value = 0;
            //nn.Item.Weight = 1;
            p1 = psum;
            w1 = c;
            b = -1;
        }

        
        private int maxProfits(ref ItemE[] tab, int f, int l, long z)
        {
            itype mp;
            item i, j, m = 0;
            stype ps;
            int d;

            for (; ; )
            {
                d = l - f + 1;
                if (d > 1)
                {
                    m = f + d / 2;
                    if (tab[f].Item.Value < tab[m].Item.Value)
                        Swap(ref tab[f], ref tab[m]);
                    if (d > 2)
                    {
                        if (tab[m].Item.Value < tab[l].Item.Value)
                        {
                            Swap(ref tab[m], ref tab[l]);
                            if (tab[f].Item.Value < tab[m].Item.Value)
                                Swap(ref tab[f], ref tab[m]);
                        }
                    }
                }
                if (d <= 3)
                    break;
                mp = tab[m].Item.Value;
                i = f;
                j = l;
                ps = 0;

                for (; ; )
                {
                    do { ps += tab[i].Item.Value; i++; } while (tab[i].Item.Value > mp);
                    do { j--;                          } while (tab[j].Item.Value < mp);
                    if (i > j)
                        break;
                    Swap(ref tab[i], ref tab[j]);
                }
                if (ps <= z)
                {
                    f = i;
                    z -= ps;
                }
                else
                    l = i - 1;
            }
            while (tab[f].Item.Value<= z)
            {
                z -= tab[f].Item.Value;
                f++;
            }
            return f;
        }

        private int minWeight(ref ItemE[] tab, int f, int l, long c)
        {
            itype mw;
            item i, j, m = 0;
            stype ws;
            int d;

            for(; ; )
            {
                d = l - f + 1;
                if(d>1)
                {
                    m = f + d / 2;
                    if (tab[f].Item.Weight > tab[m].Item.Weight)
                        Swap(ref tab[f], ref tab[m]);
                    if(d>2)
                    {
                        if (tab[m].Item.Weight > tab[l].Item.Weight)
                        {
                            Swap(ref tab[m], ref tab[l]);
                            if (tab[f].Item.Weight > tab[m].Item.Weight)
                                Swap(ref tab[f], ref tab[m]);
                        }
                    }
                }
                if (d <= 3)
                    break;
                mw = tab[m].Item.Weight;
                i = f;
                j = l;
                ws = 0; 

                for(; ; )
                {
                    do { ws += tab[i].Item.Weight; i++; } while (tab[i].Item.Weight < mw);
                    do {                           j--; } while (tab[j].Item.Weight > mw);
                    if (i > j)
                        break;
                    Swap(ref tab[i], ref tab[j]);
                }
                if (ws <= c)
                {
                    f = i;
                    c -= ws;
                }
                else
                    l = i - 1;
            }
            while(tab[f].Item.Weight <=c)
            {
                c -= tab[f].Item.Weight;
                f++;
            }
            return f;
        }

        private void DefineSolution(ref AllInfo a)
        {
            item h, m, i;
            stype psum, wsum, ws;
            btype j, k;
            item f, l;
            long t;

            if (a.fullsol==1)
            {
                for(i=a.fitem, m=a.litem+1, h=0; i!=m; i++, h++)
                {
                    a.items[i] = a.ffull[h];
                }
            }

            /* partial solutions are more difficult as we only have MAXV last changes */
            /* initialize break solution */
            for (i = a.fitem, m = a.b; i != m; i++)
                a.items[i].x = 1;
            for (i = a.b, m = a.litem + 1; i != m; i++)
                a.items[i].x = 0;

            /* backtrack vector */
            psum = a.z - a.psumb;
            wsum = a.zwsum - a.wsumb;
            f = a.fsol;
            l = a.lsol;

            /* backtrack */
            for (j = 0; j < Constants.MAXV; j++)
            {
                i = a.d.ovitem[j];
                if (i > -1)
                {
                    k = a.d.ovect & ((btype)1 << (int)j);
                    if (a.items[i].x == 1)
                    {
                        if (i > f)
                            f = i;
                        if (k != 0)
                        {
                            psum += (long)a.items[i].Item.Value;
                            wsum += (long)a.items[i].Item.Weight;
                            a.items[i].x = 0;
                        }
                    }
                    else
                    {
                        if (i < l)
                            l = i;
                        if (k != 0)
                        {
                            psum -= (long)a.items[i].Item.Value;
                            wsum -= (long)a.items[i].Item.Weight;
                            a.items[i].x = 1;
                        }
                    }
                }
            }

            if ((psum == 0) && (wsum == 0))
                return;

            f++;
            l--; /* new core */
            psum = a.z;
            wsum = a.zwsum;
            iterates++;
            if (f > l)
                Console.WriteLine("wrong backtrack");
            for (i = a.fitem, m = f; i != m; i++)
            {
                if (a.items[i].x == 1)
                {
                    psum -= a.items[i].Item.Value;
                    wsum -= a.items[i].Item.Weight;
                }
            }
            for (i = l + 1, m = a.litem + 1; i != m; i++)
            {
                if (a.items[i].x == 1)
                {
                    psum -= a.items[i].Item.Value;
                    wsum -= a.items[i].Item.Weight;
                }
            }
            for (i = f, m = l + 1, ws = 0; i != m; i++)
            {
                ws += a.items[i].Item.Weight;
            }

            if (ws == wsum)
            {
                for (i = f, m = l + 1; i != m; i++)
                {
                    a.items[i].x = 1;
                }
            }
            else
            {
                ComboSolve(ref a.items, f, l, wsum, psum - 1, psum, 1, 1);
            }
        }

        private void Rudidiv(ref AllInfo a)
        {

            //throw new NotImplementedException();
        }

        private void Heuristic(ref AllInfo a)
        {
            item i, j=-1, m;
            stype c, z, ub;
            int v, r1, rm;
            item red;
            ItemE d;
            if (a.d.size == 0)
                return;

            // deifne limits
            dynheur++;
            r1 = a.d.fset;
            rm = a.d.lset;
            c = a.c;
            z = a.z;
            ub = a.ub;


            // forward solution with dynaminc programing
            if(a.intv2  != a.intv2b)
            {
                red = a.intv[a.intv2 + 1].f;
                for(i = red, m = a.litem+1, j=-1; i!=m; i++)
                {
                    v = FindVector(ref a, c -a.items[i].Item.Weight, r1, rm);
                    if(v>=0)
                    {
                        if(a.d.set1[v].psum+ a.items[i].Item.Value > z)
                        {
                            j = i;
                            z = a.d.set1[v].psum +a.items[i].Item.Value;
                            if (z == ub)
                                break;
                        }
                    }
                }
                if(j!=-1)
                {
                    SwapOut(ref a, j, Constants.RIGHT);
                    d =a.items[red];
                    for (i = red, m = a.t; i != m; i--)
                       a.items[i] =a.items[i - 1];
                   a.items[a.t] = d;
                    a.lsort++;
                    Multiply(ref a, a.t, Constants.RIGHT);
                    a.t++;
                    ReduceSet(ref a);
                }
            }

            // backward solution 
            if (a.intv1 != a.intv1b)
            {
                red = a.intv[(a.intv1 - 1)].l;
                for(i=a.fitem, m=red+1, j=-1; i!=m; i++)
                {
                    v = FindVector(ref a, c +a.items[i].Item.Weight, r1, rm);
                    if (v >= 0)
                    {   
                        if (a.d.set1[v].psum -a.items[i].Item.Value > z)
                        {
                            j = i;
                            z = a.d.set1[v].psum -a.items[i].Item.Value;
                            if (z == ub)
                                break;
                        }
                    }
                }
                if (j != -1)
                {
                    SwapOut(ref a, j, Constants.LEFT);
                    d =a.items[red];
                    for (i = red, m = a.s; i != m; i++)
                       a.items[i] =a.items[i + 1];
                   a.items[a.s] = d;
                    a.fsort--;
                    Multiply(ref a, a.s, Constants.LEFT);
                    a.s--;
                    ReduceSet(ref a);
                }
            }
        }

        private void CleanUp(ref AllInfo a)
        {
            a.d.set1 = null;
            a.intv = null;
            //a.items = null;
            //a.d.ovitem = null;
            //a.d.vitem = null;
        }

        private bool HasChance(ref AllInfo a, item i, int side)
        {
            if (a.d.size == 0)
                return false;

            int j, m;
            itype p, w;
            stype pp, ww;

            if (side == Constants.RIGHT)
            {
                if (a.d.set1[a.d.fset].wsum <= a.c -a.items[i].Item.Weight)
                    return true;
                p = a.ps;
                w = a.ws;
                pitested++;
                pp =a.items[i].Item.Value - a.z - 1;
                ww =a.items[i].Item.Weight - a.c;

                for (j = a.d.fset, m = a.d.lset + 1; j != m; j++)
                {
                    if (DET(a.d.set1[j].psum + pp, a.d.set1[j].wsum + ww, (ntype)p, (ntype)w) >= 0)
                        return true;
                }
            }
            else // LEFT
            {
                if (a.d.set1[a.d.lset].wsum > a.c +a.items[i].Item.Weight)
                    return true;
                p = a.pt;
                w = a.wt;
                pitested++;
                pp = -a.items[i].Item.Value - a.z - 1;
                ww = -a.items[i].Item.Weight - a.c;
                for (j = a.d.lset, m = a.d.fset - 1; j != m; j--)
                {
                    if (DET(a.d.set1[j].psum + pp, a.d.set1[j].wsum + ww, (ntype)p, (ntype)w) >= 0)
                        return true;
                }
            }

            pireduced++;
            return false;
        }

        private void ReduceSet(ref AllInfo a)
        {
            int i, m, k;
            stype z, c;
            prod p, w;
            int r1, rm, v;
            boolean atstart = 1, atend = 1;
            
            if (a.d.size == 0)
                return;
            // initialize limits
            r1 = a.d.fset;
            rm = a.d.lset;
            /* find break point and improve solution */
            v = FindVector(ref a, a.c, r1, rm);
            if (v == -1) // all states infeasible
            {
                v = r1 - 1;
            }
            else
                if (a.d.set1[v].psum > a.z)
                    ImproveSolution(ref a, v);

            ExpandCore(ref a, ref atstart, ref atend);
            c = a.c;
            z = a.z + 1;
            k = a.d.setm;
            
            if(atstart==0)
            {
                p = a.ps;
                w = a.ws;
                for (i = rm, m = v; i != m; i--)
                {
                    if (DET(a.d.set1[i].psum-z, a.d.set1[i].wsum-c, (ntype)p, (ntype)w) >= 0)
                    {
                        k--;
                        a.d.set1[k] = a.d.set1[i];
                    }
                }
            }
            
            if(atend == 0)
            {
                p = a.pt;
                w = a.wt;
                for (i = v, m = r1 - 1; i != m; i--)
                {
                    if (DET(a.d.set1[i].psum-z, a.d.set1[i].wsum-c, (ntype)p, (ntype)w) >= 0)
                    {
                        k--;
                        a.d.set1[k] = a.d.set1[i];
                    }
                }
            }
            
            // save limit
            a.d.fset = k;
            a.d.lset = a.d.setm - 1;
            a.d.size = a.d.lset - a.d.fset + 1;
        }

        private void SimpReduced(ref AllInfo a, int side, ref int f, ref int l)
        {
            item i, j, k;
            ntype pb, wb;
            ptype q;
            int red;

            if (a.d.size == 0)
            {
                f = l + 1;
                return;
            }
            if (l < f)
                return;
            pb =a.items[a.b].Item.Value;
            wb =a.items[a.b].Item.Weight;

            q = DET(a.z + 1 - a.psumb, a.c - a.wsumb, (ntype)pb, (ntype)wb);
            
            i = f;
            j = l;

            red = 0;
            if (side == Constants.LEFT)
            {
                k = a.fsort - 1;
                while (i != j+1)
                {
                    if (DET(-a.items[j].Item.Value, -a.items[j].Item.Weight, (ntype)pb, (ntype)wb) < q)
                    {
                        // not feasible
                        Swap(ref a.items[i], ref a.items[j]);
                        i++;
                        red++;
                    }
                    else
                    {
                        // feasible
                        Swap(ref a.items[j], ref a.items[k]);
                        j--;
                        k--;
                    }
                }
                l = a.fsort - 1;
                f = k + 1;
            }
            else
            {
                k = a.lsort + 1;
                while (i != j+1)
                {
                    if (DET(a.items[i].Item.Value,a.items[i].Item.Weight, pb, wb) < q)
                    {
                        Swap(ref a.items[i], ref a.items[j]);
                        j--;
                        red++;
                    }
                    else
                    {
                        Swap(ref a.items[i], ref a.items[k]);
                        i++;
                        k++;
                    }
                }
                f = a.lsort + 1;
                l = k - 1;
            }
            if(a.master ==1)
                simpreduced += red;
        }

        private void ExpandCore(ref AllInfo a, ref int atstart, ref int atend)
        {
            item f=0, l=0;
            atstart = 0;
            if (a.s < a.fsort)
            {
                if (a.intv1 == a.intv1b)
                {
                    atstart = 1;
                }
                else
                {
                    Pop(ref a, Constants.LEFT, ref f, ref l);
                    a.ps =a.items[f].Item.Value;
                    a.ws =a.items[f].Item.Weight;
                    SimpReduced(ref a, Constants.LEFT, ref f, ref l);
                    if(f != l+1)
                    {
                        PartSort(ref a, f, l, 0, 0, Constants.SORTALL);
                        a.fsort = f;
                        a.ps =a.items[a.s].Item.Value;
                        a.ws =a.items[a.s].Item.Weight;
                    }
                }
            } else
            {
                a.ps =a.items[a.s].Item.Value;
                a.ws =a.items[a.s].Item.Weight;
            }
            //expand core
            atend = 0;
            if(a.t > a.lsort)
            {
                if(a.intv2 == a.intv2b)
                {
                    atend = 1;
                }
                else
                {
                    Pop(ref a, Constants.RIGHT, ref f, ref l);
                    a.pt =a.items[l].Item.Value;
                    a.wt =a.items[l].Item.Weight;
                    SimpReduced(ref a, Constants.RIGHT, ref f, ref l);
                    if(f != l + 1)
                    {
                        PartSort(ref a, f, l, 0, 0, Constants.SORTALL);
                        a.lsort = l;
                        a.pt =a.items[a.t].Item.Value;
                        a.wt =a.items[a.t].Item.Weight;
                    }
                }
            } else
            {
                a.pt =a.items[a.t].Item.Value;
                a.wt =a.items[a.t].Item.Weight;
            }
        }

        private int FindVector(ref AllInfo a, long ws, int f, int l)
        {
            /* find vector i, so that i->wsum <= ws < (i+1)->wsum */
            int m;
            if (f > l)
                Console.WriteLine("findvect: empty set");
            if (a.d.set1[f].wsum > ws)
                return -1;
            if (a.d.set1[l].wsum <= ws)
                return l;

            while (l - f > Constants.SYNC)
            {
                m = f + (l - f) / 2;
                if (a.d.set1[m].wsum > ws)
                    l = m - 1;
                else
                    f = m;
            }
            while (a.d.set1[l].wsum > ws) l--;
            return l;
        }

        private void FindCore(ref AllInfo a)
        {
            item i, m;
            stype p, r;
            item j, s, t, b;

            /* alla.items apart from b must be in intervals */
            s = t = b = a.b;
            if (a.fpart <= b - 1)
                Push(ref a, Constants.LEFT, a.fpart, b - 1);
            if (b + 1 <= a.lpart)
                Push(ref a, Constants.RIGHT, b + 1, a.lpart);

            /* initial core is b-1, b, b+1 (if these exist) */
            if(b-1 >= a.fitem)
            {
                SwapOut(ref a, b - 1, Constants.LEFT);
                s--;
            }
            if (b + 1 <= a.litem)
            {
                SwapOut(ref a, b + 1, Constants.RIGHT);
                t++;
            }

            // forward greedy solution
            if (b - 1 >= a.fitem)
            {
                p = 0;
                r = a.c - a.wsumb +a.items[b-1].Item.Weight;
                for(i = t + 1, m = a.litem+1, j = -1; i != m; i++)
                {
                    if((a.items[i].Item.Weight <= r) && (a.items[i].Item.Value > p))
                    {
                        p =a.items[i].Item.Value;
                        j = i;
                    }
                }
                if(j != -1)
                {
                    SwapOut(ref a, j, Constants.RIGHT);
                    t++;
                }
            }

            /* second forward greedy solution */
            p = 0;
            r = a.c - a.wsumb;
            for(i = t+1, m = a.litem+1, j = -1; i!=m; i++)
            {
                if((a.items[i].Item.Weight <= r) && (a.items[i].Item.Value > p))
                {
                    p =a.items[i].Item.Value;
                    j = i;
                }
            }
            if (j != -1)
            {
                SwapOut(ref a, j, Constants.RIGHT);
                t++;
            }
            // backward greedy solution
            j = -1;
            r = a.wsumb - a.c +a.items[b].Item.Weight;
            for (i = a.fitem, m = s; i != m; i++)
            {
                if (a.items[i].Item.Weight >= r)
                {
                    p =a.items[i].Item.Value + 1;
                    break;
                }
            }

            for (; i != m; i++)
            {
                if ((a.items[i].Item.Weight >= r) && (a.items[i].Item.Value < p))
                {
                    p =a.items[i].Item.Value;
                    j = i;
                }
            }

            if (j != -1)
            {
                SwapOut(ref a, j, Constants.LEFT);
                s--;
            }

            /* second backward solution */
            if (b + 1 <= a.litem)
            {
                j = -1;
                r = a.wsumb - a.c +a.items[b].Item.Weight+ a.items[b+1].Item.Weight;

                for (i = a.fitem, m = s; i != m; i++)
                {
                    if (a.items[i].Item.Weight >= r)
                    {
                        p =a.items[i].Item.Value + 1;
                        break;
                    }
                }

                for (; i != m; i++)
                {
                    if ((a.items[i].Item.Weight >= r) && (a.items[i].Item.Value < p))
                    {
                        p =a.items[i].Item.Value;
                        j = i;
                    }
                }
            }

            /* add first and last item to ensure some variation in weights */
            if (a.fitem < s)
            {
                s--;
                SwapOut(ref a, a.fitem, Constants.LEFT);
            }

            if (a.litem > t)
            {
                t++;
                SwapOut(ref a, a.litem, Constants.RIGHT);
            }

            /* enumerate core: reductions are not allowed! */
            InitFirst(ref a, a.psumb, a.wsumb);
            Moveset(ref a);
            for (i = b, j = b - 1; (i <= t) || (j >= s);)
            {
                if (i <= t)
                {
                    Multiply(ref a, i, Constants.RIGHT);
                    Moveset(ref a);
                    i++;
                }

                if (j >= s)
                {
                    Multiply(ref a, j, Constants.LEFT);
                    Moveset(ref a);
                    j--;
                }
            }

            a.s = s - 1;
            a.fsort = s;
            a.t = t + 1;
            a.lsort = t;
        }

        private void Multiply(ref AllInfo a, int h, int side)
        {
            //states
            int i_state, j_state, k_state, m_state;
            itype p, w;
            btype mask0, mask1;
            int r1_state, rm_state;
            if (a.d.size == 0)
                return;
            if (side == Constants.RIGHT)
            {
                p =a.items[h].Item.Value;
                w =a.items[h].Item.Weight;
            }
            else
            {
                p = -a.items[h].Item.Value;
                w = -a.items[h].Item.Weight;
            }

            /* keep track on solution vector */
            a.d.vno++;
            if (a.d.vno == Constants.MAXV)
                a.d.vno = 0;
            mask1 = (btype)1 << (int)a.d.vno;
            mask0 = ~mask1;

            a.d.vitem[a.d.vno] = h;
            /* initialize limits */

            r1_state = a.d.fset;
            rm_state = a.d.lset;
            k_state = 0;
            m_state = rm_state + 1;

            a.d.set1[k_state].psum = -1;
            a.d.set1[k_state].wsum = a.d.set1[r1_state].wsum +a.items[h].Item.Weight + 1;
            a.d.set1[m_state].wsum = a.d.set1[rm_state].wsum +a.items[h].Item.Weight + 1;

            for (i_state = r1_state, j_state = r1_state; (i_state != m_state) || (j_state != m_state);)
            {
                if (a.d.set1[i_state].wsum <= a.d.set1[j_state].wsum + w)
                {
                    if (a.d.set1[i_state].psum > a.d.set1[k_state].psum)
                    {
                        if (a.d.set1[i_state].wsum > a.d.set1[k_state].wsum)
                        {
                            k_state++;
                            if((k_state==i_state)||(k_state==j_state))
                                break;
                        }

                        a.d.set1[k_state].psum = a.d.set1[i_state].psum;
                        a.d.set1[k_state].wsum = a.d.set1[i_state].wsum;
                        a.d.set1[k_state].vect = a.d.set1[i_state].vect & mask0;
                    }
                    i_state++;
                }
                else
                {
                    if (a.d.set1[j_state].psum + p > a.d.set1[k_state].psum)
                    {
                        if (a.d.set1[j_state].wsum + w > a.d.set1[k_state].wsum)
                        {
                            k_state++;
                            if ((k_state == i_state) || (k_state == j_state))
                                break;
                        }
                        a.d.set1[k_state].psum = a.d.set1[j_state].psum + p;
                        a.d.set1[k_state].wsum = a.d.set1[j_state].wsum + w;
                        a.d.set1[k_state].vect = a.d.set1[j_state].vect | mask1;
                    }

                    j_state++;
                }
            }
            if((k_state == i_state) || (k_state == j_state))
                Console.WriteLine("multiply, no space");

            a.d.fset = 0;
            a.d.lset = k_state;
            a.d.size = a.d.lset - a.d.fset + 1;
            
            if (a.d.size > maxstates)
                maxstates = a.d.size;
            a.coresize++;
            if (a.master != 0)
                coresize++;
        }

        private void Moveset(ref AllInfo a)
        {
            int i, j, m;
            //d = a.d;
            /* move array to end if necessary */
            if (a.d.lset != a.d.setm - 1)
            {
                for (i = a.d.setm, j = a.d.lset, m = a.d.fset - 1; j != m; j--)
                {
                    i--;
                    a.d.set1[i] = a.d.set1[j];
                }

                a.d.fset = i;
                a.d.lset = a.d.setm - 1;
            }
        }

        private void InitFirst(ref AllInfo a, long pb, long wb)
        {
            State k;
            btype i;

            /* create table */

            a.d.set1 = new State[Constants.MAXSTATES];
            a.d.setm = Constants.MAXSTATES - 1;
            a.d.size = 1;
            a.d.fset = 0;
            a.d.lset = 0;

            /* init first state */
            
            a.d.set1[a.d.fset].psum = pb;
            a.d.set1[a.d.fset].wsum = wb;
            a.d.set1[a.d.fset].vect = 0;

            /* init solution vector */
            for (i = 0; i < Constants.MAXV; i++)
                a.d.vitem[i] = -1;
            a.d.vno = Constants.MAXV - 1;

            /* init full solution */
            a.fullsol = 0;
            a.ffull = new ItemE[a.litem+1-a.fitem];
            a.lfull = a.fullsol + a.litem - a.fitem + 1;
        }

        private void SwapOut(ref AllInfo a, item i, int side)
        {
            int pos, k;
            if(side == Constants.LEFT)
            {
                for(pos = a.intv1b; pos!=a.intv1; pos++)
                {
                    if ((a.intv[pos].f <= i) && (i <= a.intv[pos].l))
                    {
                        Swap(ref a.items[i], ref a.items[a.intv[pos].l]);
                        a.intv[pos].l--;
                        pos++;
                        break;
                    }
                }
                for(;pos != a.intv1; pos++)
                {
                    Swap(ref a.items[a.intv[pos].f - 1], ref a.items[a.intv[pos].l]);
                    a.intv[pos].f--;
                    a.intv[pos].l--;
                }
                // remove empty intervals
                for(pos = k = a.intv1b; pos!= a.intv1; pos++)
                {
                    if(a.intv[pos].f <= a.intv[pos].l)
                    {
                        k = pos;
                        k++;
                    }
                }
                a.intv1 = k;
            } else
            {
                for (pos = a.intv2b; pos != a.intv2; pos--)
                {
                    if ((a.intv[pos].f <= i) && (i <= a.intv[pos].l))
                    {
                        Swap(ref a.items[i], ref a.items[a.intv[pos].f]);
                        a.intv[pos].f++;
                        pos--;
                        break;
                    }
                }
                for (; pos != a.intv2; pos--)
                {
                    Swap(ref a.items[a.intv[pos].f], ref a.items[a.intv[pos].l + 1]);
                    a.intv[pos].f++;
                    a.intv[pos].l++;
                }
                // remove empty intervals
                for (pos = k = a.intv2b; pos != a.intv2; pos--)
                {
                    if (a.intv[pos].f <= a.intv[pos].l)
                    {
                        k = pos;
                        k--;
                    }
                }
                a.intv2 = k;
            }
        }

        private void FindBreak(ref AllInfo a)
        {
            item i;
            stype psum, wsum, r;

            psum = 0;
            r = a.c;
            for (i = a.fitem;a.items[i].Item.Weight <=r; i++)
            {
                psum +=a.items[i].Item.Value;
                r -=a.items[i].Item.Weight;
            }

            wsum = a.c - r;

            a.b = i;
            a.psumb = psum;
            a.wsumb = wsum;
            a.dantzig = psum + (r *a.items[i].Item.Value)/ a.items[i].Item.Weight;

        }

        #region push/pop intervals part sort
        private void Push(ref AllInfo a, int side, item f, item l)
        {
            int pos = 0;

            switch (side)
            {
                case Constants.LEFT:
                    pos = a.intv1++;
                    break;
                case Constants.RIGHT:
                    pos = a.intv2--;
                    break;
            }
            if (a.intv1 == a.intv2)
                Console.WriteLine("interval stack full");
            a.intv[pos].f = f;
            a.intv[pos].l = l;
        }

        private void Pop(ref AllInfo a, int side, ref item f, ref item l)
        {
            int pos = 0;
            switch (side)
            {
                case Constants.LEFT:
                    if (a.intv1 == a.intv1b)
                        Console.WriteLine("Err pop left");

                    pos = --a.intv1;
                    break;
                case Constants.RIGHT:
                    if (a.intv2 == a.intv2b)
                        Console.WriteLine("Err pop right");
                    pos = ++a.intv2;
                    break;
            }
            // TODO pos = -1
            f = a.intv[pos].f;
            l = a.intv[pos].l;
        }
        #endregion

        private void ImproveSolution(ref AllInfo a, int v)
        {
            if (a.d.set1[v].wsum > a.c)
                Console.WriteLine("wrong improve solution");
            if (a.d.set1[v].psum <= a.z)
                Console.WriteLine("not improved solution");
            a.z = a.d.set1[v].psum;
            a.zwsum = a.d.set1[v].wsum;
            a.fsol = a.s;
            a.lsol = a.t;
            a.d.ovect = a.d.set1[v].vect;
            a.fullsol = 0;
            Array.Copy(a.d.vitem, a.d.ovitem, Constants.MAXV);
        }

        #region PartSort

        private void PartSort(ref AllInfo a, int f, int l, stype ws, stype c, int what)
        {
            stype mp, mw;
            item i, j, m = 0;
            Item mm;
            stype wi;
            item d = l - f + 1;
            if (d < 1)
                Console.WriteLine("negative interval in partsort");
            if (d > 1)
            {
                m = f + d / 2;
                if (DET(a.items[f],a.items[m]) < 0)
                    Swap(ref a.items[f], ref a.items[m]);
                if (d > 2)
                    if (DET(a.items[m],a.items[l]) < 0)
                    {

                        Swap(ref a.items[m], ref a.items[l]);

                        if (DET(a.items[f],a.items[m]) < 0)
                            Swap(ref a.items[f], ref a.items[m]);
                    }

            }


            mm =a.items[m].Item;


            if (d > 3)
            {
                mp = mm.Value;
                mw = mm.Weight;
                i = f;
                j = l;
                wi = ws;
                while (true)
                {
                    do
                    {
                        wi +=a.items[i].Item.Weight;
                        i++;
                    } while (DET(a.items[i].Item.Value,a.items[i].Item.Weight, mp, mw) > 0);

                    do
                    {
                        j--;
                    } while (DET(a.items[j].Item.Value,a.items[j].Item.Weight, mp, mw) < 0);

                    if (i > j)
                        break;

                    Swap(ref a.items[i], ref a.items[j]);
                    //Console.WriteLine("swap\t" +a.items[i].Item.GetInfo() + "\t" +a.items[j].Item.GetInfo());
                }
                //Console.WriteLine("End\n");

                if (wi <= c)
                {
                    if (what == Constants.SORTALL)
                        PartSort(ref a, f, i - 1, ws, c, what);
                    if (what == Constants.PARTIATE)
                        Push(ref a, Constants.LEFT, f, i - 1);
                    PartSort(ref a, i, l, wi, c, what);
                }
                else
                {
                    if (what == Constants.SORTALL)
                        PartSort(ref a, i, l, wi, c, what);
                    if (what == Constants.PARTIATE)
                        Push(ref a, Constants.RIGHT, i, l);
                    PartSort(ref a, f, i - 1, ws, c, what);
                }
            }

            if ((d <= 3) || (what == Constants.SORTALL))
            {
                a.fpart = f;
                a.lpart = l;
                a.wfpart = ws;
            }
        }

        #endregion

        #region DET/SWAP
        private ptype DET(Item item1, Item item2)
        {
            //if ((item1.Value * item2.Weight - item2.Value * item1.Weight) != (double)item1.Value * (double)item2.Weight - (double)item2.Value * (double)item1.Weight)
            //    return 0;
            return (double)item1.Value * (double)item2.Weight - (double)item2.Value * (double)item1.Weight;
        }
        private ptype DET(ItemE item1, ItemE item2)
        {
            return (double)item1.Item.Value * (double)item2.Item.Weight - (double)item2.Item.Value * (double)item1.Item.Weight;
        }

        private ptype DET(ntype a, ntype b, prod c, prod d)
        {
            return (double)a * (double)d - (double)b * (double)c;
        }

        private ptype DET(ntype a, ntype b, ntype c, ntype d)
        {
            return (double)a * (double)d - (double)b * (double)c;
        }
        private ptype DET(stype a, stype b, ntype c, ntype d)
        {
            return ((double)a * (double)d) - (double)b * (double)c;
        }
        private ptype DET(ntype a, ntype b, stype c, stype d)
        {
            return ((double)a * (double)d) - (double)b * (double)c;
        }

        private void Swap(ref ItemE item1, ref ItemE item2)
        {
            ItemE buff = item1;
            item1 = item2;
            item2 = buff;
        }
        #endregion

        private void printItems(AllInfo a)
        {
            Console.WriteLine("Weight\tValue");
            for(int i = 0; i< a.items.Length;i++)
            {
                Console.WriteLine(a.items[i].Item.GetInfo());
            }
        }

        public void SetCustomParameters(object[] objects)
        {

        }
    }


}
