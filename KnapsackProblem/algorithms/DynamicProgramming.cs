using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;
using System.Collections.Generic;

namespace KnapsackProblem.algorithms
{
    public class DynamicProgramming : IKnapsackSolver
    {
        public string GetName()
        {
            return "Dynamic programming";
        }

        public List<string> GetCustomParameters()
        {
            return null;
        }

        public List<string> GetCustomParametersNames()
        {
            return null;
        }

        public IKnapsackResult Solve(IKnapsack discreteKnapsack)
        {
            DiscreteKnapsack knapsack = (DiscreteKnapsack)discreteKnapsack;
            int[,] sumOfValues = new int[knapsack.Items.Length + 1, knapsack.Capacity + 1];
            int[,] lastItem = new int[knapsack.Items.Length + 1, knapsack.Capacity + 1];

            for (int k = 0; k < knapsack.Items.Length + 1; k++)
            {
                lastItem[k, 0] = -1;
            }
            for (long k = 0; k < knapsack.Capacity + 1; k++)
            {
                lastItem[0, k] = -1;
            }

            for (int i = 1; i <= knapsack.Items.Length; i++)
            {
                int idItem = i - 1;

                for (int j = 1; j <= (int)knapsack.Capacity; j++)
                {
                    
                    if (knapsack.Items[idItem].Weight > j)
                    {
                        sumOfValues[i, j] = sumOfValues[i - 1, j];
                        lastItem[i, j] = lastItem[i - 1, j];
                    }
                    else
                    {
                        int value1 = sumOfValues[i - 1, j];
                        int value2 = sumOfValues[i - 1, j - knapsack.Items[idItem].Weight] + knapsack.Items[idItem].Value;
                        if (value1 > value2)
                        {
                            sumOfValues[i, j] = value1;
                            lastItem[i, j] = lastItem[i - 1, j];
                        }
                        else
                        {
                            sumOfValues[i, j] = value2;
                            lastItem[i, j] = i;
                        }
                    }
                }
            }

            List<Item> items = new List<Item>();
            int firstIndex = knapsack.Items.Length;
            int secondIndex = (int)knapsack.Capacity;
            while (lastItem[firstIndex, secondIndex] != -1)
            {
                if (firstIndex == lastItem[firstIndex, secondIndex])
                {
                    items.Add(knapsack.Items[firstIndex - 1]);
                    secondIndex = secondIndex - knapsack.Items[firstIndex - 1].Weight;
                    firstIndex--;
                }
                else
                {
                    firstIndex = lastItem[firstIndex, secondIndex];
                }
            }

            KnapsackResult knapsackResult = new KnapsackResult(
                items,
                (long)sumOfValues[knapsack.Items.Length, knapsack.Capacity]);
            return knapsackResult;
        }

        public void SetCustomParameters(object[] objects)
        {

        }
    }
}
