﻿using KnapsackProblem.data.models.discreteKnapsackProblem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.generator
{
    class DiscreteItemsListGenerator
    {
        Random random = new Random();

        public void SetSeed(int seed)
        {
            random = new Random(seed);
        }

        public Item[] GenerateList(DataType dataType, int amountOfItems, int range)
        {
            Item[] items;
            switch (dataType)
            {
                case DataType.UncorrelatedDataInstances:
                    items = UncorrelatedData(amountOfItems, range);
                    break;
                case DataType.WeaklyCorrelatedInstances:
                    items = WeaklyCorrelated(amountOfItems, range);
                    break;
                case DataType.StronglyCorrelatedInstances:
                    items = StronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.InverseStronglyCorrelatedInstances:
                    items = InverseStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.AlmostStronglyCorrelatedInstances:
                    items = AlmostStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.SubsetSumInstances:
                    items = SubsetSumInstances(amountOfItems, range);
                    break;
                default:
                    return null;
            }
            return items;
        }

        private Item[] UncorrelatedData(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(1, range);

                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] WeaklyCorrelated(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight - range / 10, weight + range / 10);
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] StronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight + range / 10;
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] InverseStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int value = random.Next(1, range);
                int weight = value + range / 10;
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] AlmostStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight + range / 10 - range / 500, weight + range / 10 + range / 500);
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] SubsetSumInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight;
                items[i] = (new Item(weight, value));
            }
            return items;
        }
    }
}
