using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using System;
using System.Collections.Generic;

namespace KnapsackProblem.data.generator
{
    public class DiscreteKnapsackGenerator : IKnapsackGenerator
    {
        private Random random;

        public DiscreteKnapsackGenerator()
        {
            random = new Random();
        }

        public void SetSeed(int seed)
        {
            random = new Random(seed);
        }

        public DiscreteKnapsack GenerateKnapsackWithCalculatedCapacity(int amountOfItems, int range, double percentCapacity, DataType dataType)
        {
            DiscreteKnapsack discreteKnapsack;
            Item[] items;
            switch (dataType)
            {
                case DataType.UncorrelatedDataInstances:
                    items = UncorrelatedData(amountOfItems, range);
                    break;
                case DataType.WeaklyCorrelatedInstances:
                    items = WeaklyCorrelated(amountOfItems, range);
                    break;
                case DataType.StronglyCorrelatedInstances:
                    items = StronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.InverseStronglyCorrelatedInstances:
                    items = InverseStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.AlmostStronglyCorrelatedInstances:
                    items = AlmostStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.SubsetSumInstances:
                    items = SubsetSumInstances(amountOfItems, range);
                    break;
                default:
                    return null;
            }
            long weightSum = 0;
            for(int i = 0; i < amountOfItems; i++)
            {
                weightSum += items[i].Weight;
            }
            discreteKnapsack = new DiscreteKnapsack((long)(percentCapacity*(double)weightSum/100), items);
            return discreteKnapsack;
        }

        public DiscreteKnapsack GenerateKnapsack(int amountOfItems, int range, long capacity, DataType dataType)
        {
            DiscreteKnapsack discreteKnapsack;
            Item[] items;
            switch (dataType)
            {
                case DataType.UncorrelatedDataInstances:
                    items = UncorrelatedData(amountOfItems, range);
                    break;
                case DataType.WeaklyCorrelatedInstances:
                    items = WeaklyCorrelated(amountOfItems, range);
                    break;
                case DataType.StronglyCorrelatedInstances:
                    items = StronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.InverseStronglyCorrelatedInstances:
                    items = InverseStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.AlmostStronglyCorrelatedInstances:
                    items = AlmostStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.SubsetSumInstances:
                    items = SubsetSumInstances(amountOfItems, range);
                    break;
                default:
                    return null;
            }

            discreteKnapsack = new DiscreteKnapsack(capacity, items);
            return discreteKnapsack;
        }

        private Item[] UncorrelatedData(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(1, range);

                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] WeaklyCorrelated(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight - range / 10, weight + range / 10);
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] StronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight + range / 10;
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] InverseStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int value = random.Next(1, range);
                int weight = value + range / 10;
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] AlmostStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight + range / 10 - range / 500, weight + range / 10 + range / 500);
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        private Item[] SubsetSumInstances(int amountOfItems, int range)
        {
            Item[] items = new Item[amountOfItems];
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight;
                items[i] = (new Item(weight, value));
            }
            return items;
        }

        public IKnapsack Generate(int numersOfItems, object[] parameters)
        {
            if (parameters.Length != 3)
                throw new Exception("Wrong number of parameters");
            else
            {
                object sa = 3;
                var c = (int)parameters[0];
                var cc = (double) parameters[1];

                return GenerateKnapsackWithCalculatedCapacity(
                    numersOfItems,
                    (int) parameters[0],
                    (double) parameters[1],
                    (DataType) parameters[2]);
            }
        }

        public List<string> GetParametersNames()
        {
            List<string> param = new List<string>();
            param.Add("range");
            param.Add("% capacity");
            param.Add("data type");
            return param;
        }
    }
}
