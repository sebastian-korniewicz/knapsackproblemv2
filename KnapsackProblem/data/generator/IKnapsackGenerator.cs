﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;

namespace KnapsackProblem.data.generator
{
    public interface IKnapsackGenerator
    {
        IKnapsack Generate(int numersOfItems, Object[] objects);
        List<string> GetParametersNames();
        void SetSeed(int seed);
    }
}
