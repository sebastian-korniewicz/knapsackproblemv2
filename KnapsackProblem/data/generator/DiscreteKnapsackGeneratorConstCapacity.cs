﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;

namespace KnapsackProblem.data.generator
{
    public class DiscreteKnapsackGeneratorConstCapacity : IKnapsackGenerator
    {
        private DiscreteItemsListGenerator listGenerator = new DiscreteItemsListGenerator();
        public IKnapsack Generate(int numersOfItems, object[] parameters)
        {
            if (parameters.Length != 3)
                throw new Exception("Wrong number of parameters");
            else
            {
                return GenerateKnapsackConstCapacity(
                    numersOfItems,
                    (int)parameters[0],
                    (long)parameters[1],
                    (DataType)parameters[2]);
            }
        }

        public DiscreteKnapsack GenerateKnapsackConstCapacity(int amountOfItems, int range, long capacity, DataType dataType)
        {
            DiscreteKnapsack discreteKnapsack;
            var items = listGenerator.GenerateList(dataType, amountOfItems, range);

            discreteKnapsack = new DiscreteKnapsack(capacity, items);
            return discreteKnapsack;
        }

        public List<string> GetParametersNames()
        {
            List<string> param = new List<string>();
            param.Add("range");
            param.Add("capacity");
            param.Add("data type");
            return param;
        }

        public void SetSeed(int seed)
        {
            listGenerator.SetSeed(seed);
        }
    }
}
