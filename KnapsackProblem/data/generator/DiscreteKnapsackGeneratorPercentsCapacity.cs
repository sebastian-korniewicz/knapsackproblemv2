using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using System;
using System.Collections.Generic;

namespace KnapsackProblem.data.generator
{
    public class DiscreteKnapsackGeneratorPercents : IKnapsackGenerator
    { 
        private DiscreteItemsListGenerator listGenerator = new DiscreteItemsListGenerator();
        public void SetSeed(int seed)
        {
            listGenerator.SetSeed(seed);
        }

        public DiscreteKnapsack GenerateKnapsackWithCalculatedCapacity(int amountOfItems, int range, double percentCapacity, DataType dataType)
        {
            Item[] items = listGenerator.GenerateList(dataType, amountOfItems, range);

            long weightSum = 0;
            for(int i = 0; i < amountOfItems; i++)
            {
                weightSum += items[i].Weight;
            }
            DiscreteKnapsack discreteKnapsack = new DiscreteKnapsack((long)(percentCapacity*(double)weightSum/100), items);

            return discreteKnapsack;
        }

        

        public IKnapsack Generate(int numersOfItems, object[] parameters)
        {
            if (parameters.Length != 3)
                throw new Exception("Wrong number of parameters");
            else
            {
                return GenerateKnapsackWithCalculatedCapacity(
                    numersOfItems,
                    (int) parameters[0],
                    (double) parameters[1],
                    (DataType) parameters[2]);
            }
        }

        public List<string> GetParametersNames()
        {
            List<string> param = new List<string>();
            param.Add("range");
            param.Add("% capacity");
            param.Add("data type");
            return param;
        }
    }
}
