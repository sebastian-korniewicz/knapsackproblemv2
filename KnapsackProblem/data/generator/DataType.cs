﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.generator
{
    public enum DataType
    {
        UncorrelatedDataInstances,
        WeaklyCorrelatedInstances,
        StronglyCorrelatedInstances,
        InverseStronglyCorrelatedInstances,
        AlmostStronglyCorrelatedInstances,
        SubsetSumInstances,
        UncorrelatedInstancesWithSimilarweights
    }

    public class DataTypeConverter
    {
        public static string GetName(DataType dataType)
        {
            switch (dataType)
            {
                case DataType.UncorrelatedDataInstances:
                    return "Uncorrelated";
                case DataType.WeaklyCorrelatedInstances:
                    return "Weakly correlated";
                case DataType.StronglyCorrelatedInstances:
                    return "Strongly correlated";
                case DataType.InverseStronglyCorrelatedInstances:
                    return "Inverse strongly correlated";
                case DataType.AlmostStronglyCorrelatedInstances:
                    return "Almost strongly correlated";
                case DataType.SubsetSumInstances:
                    return "Subset";
                case DataType.UncorrelatedInstancesWithSimilarweights:
                    return "Uncorrelated with similar weight";
            }

            return "null";
        }
    } 
}
    
