﻿using KnapsackProblem.algorithms.Combo;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;
using KnapsackProblem.validatorResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.validatorResult
{
    public class FullValidatorCombo : IKnapsackValidator
    {
        private DiscreteKnapsack _discreteKnapsack;
        private KnapsackResult optimalResult;

        public void ClearMemory()
        {
            _discreteKnapsack = null;
            optimalResult = null;
        }

        public List<string> ColumnsInfo()
        {
            return new List<string> { "optimal", "weight error", "value error" };
        }

        public void SetKnapsack(IKnapsack discreteKnapsack)
        {
            Combo combo = new Combo();
            this._discreteKnapsack = (DiscreteKnapsack)discreteKnapsack;
            optimalResult = (KnapsackResult)combo.Solve(discreteKnapsack);
        }

        public List<string> ValidateSolution(IKnapsackResult knapsackResult)
        {
            KnapsackResult discreteKnapsackResult = (KnapsackResult)knapsackResult;
            List<string> ret = new List<string>();
            if (discreteKnapsackResult.SumOfValue == optimalResult.SumOfValue)
                ret.Add("true");
            else
                ret.Add("false");
            Int64 sumOfValue = 0;
            Int64 sumOfWeight = 0;
            foreach (var item in discreteKnapsackResult.Items)
            {
                sumOfValue += (long)item.Value;
                sumOfWeight += (long)item.Weight;
            }
            if (sumOfValue != discreteKnapsackResult.SumOfValue)
                ret.Add("true");
            else
                ret.Add("false");
            if (sumOfWeight > _discreteKnapsack.Capacity)
                ret.Add("true");
            else
                ret.Add("false");
            return ret;
        }
    }
}
