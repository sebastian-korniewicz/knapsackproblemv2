﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.validatorResult
{
    public interface IKnapsackValidator
    {
        List<string> ValidateSolution(IKnapsackResult knapsackResult);
        List<string> ColumnsInfo();
        void SetKnapsack(IKnapsack knapsack);
        void ClearMemory();
    }
}
