﻿//using System;
//using System.Collections.Generic;
//using KnapsackProblem.data.models;
//using KnapsackProblem.algorithms;
//using KnapsackProblem.data.models.interfaces;

//namespace KnapsackProblem.validatorResult
//{

//    public class OptimalKnapsackValidatorMinknap : IKnapsackValidator
//    {
//        private DiscreteKnapsack _discreteKnapsack;
//        private KnapsackResult optimalResult;

//        private int Verification(DiscreteKnapsack discreteKnapsack, KnapsackResult bestKnapsackResult,
//            KnapsackResult currentKnapsackResult)
//        {
//            int sumOfErrors = 0;
//            // not optimal result
//            if (bestKnapsackResult.SumOfValue>currentKnapsackResult.SumOfValue)
//                sumOfErrors += 1;

//            // searching for duplicates and incorrect items
//            List<Item> allItems = new List<Item>();
//            foreach (var item in discreteKnapsack.Items)
//                allItems.Add(item);

//            foreach (var item in currentKnapsackResult.Items)
//            {
//                if (allItems.Contains(item))
//                    allItems.Remove(item);
//                else
//                {
//                    sumOfErrors += 2;
//                    return sumOfErrors;
//                }
//            }

//            // check sum of values and weight
//            Int64 sumOfValue = 0;
//            Int64 sumOfWeight = 0;
//            foreach (var item in currentKnapsackResult.Items)
//            {
//                sumOfValue += (long)item.Value;
//                sumOfWeight += (long)item.Weight;
//            }

//            if (currentKnapsackResult.SumOfValue != sumOfValue)
//                sumOfErrors += 4;

//            if (sumOfWeight>discreteKnapsack.Capacity)
//                sumOfErrors += 8;

//            // return -1 if there is better solution
//            if (sumOfErrors==0 && sumOfValue>bestKnapsackResult.SumOfValue)
//                return -1;

//            return sumOfErrors;
//        }

//        public string ColumnsInfo()
//        {
//            return "sum of errors";
//        }

//        public void SetKnapsack(IKnapsack discreteKnapsack)
//        {
//            Minknap minknap = new Minknap();
//            optimalResult = (KnapsackResult)minknap.Solve(discreteKnapsack);
//        }

//        public string ValidateSolution(IKnapsackResult knapsackResult)
//        {

//            return Verification(_discreteKnapsack, optimalResult, (KnapsackResult)knapsackResult).ToString();
//        }
//    }
//}
