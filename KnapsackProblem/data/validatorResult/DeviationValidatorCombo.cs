﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.algorithms.Combo;
using KnapsackProblem.data.models;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.validatorResult
{
    public class DeviationValidatorCombo : IKnapsackValidator
    {
        private DiscreteKnapsack _discreteKnapsack;
        private KnapsackResult optimalResult;

        public void ClearMemory()
        {
            _discreteKnapsack = null;
            optimalResult = null;
        }

        public List<string> ColumnsInfo()
        {
            return new List<string> {"deviation from optimal"};
        }

        public void SetKnapsack(IKnapsack discreteKnapsack)
        {
            Combo combo = new Combo();
            optimalResult = (KnapsackResult)combo.Solve(discreteKnapsack);
        }

        public List<string> ValidateSolution(IKnapsackResult knapsackResult)
        {
            KnapsackResult discreteKnapsackResult = (KnapsackResult) knapsackResult;
            if (0 == optimalResult.SumOfValue)
                return new List<string> { "0%" };
            double error = ((double)optimalResult.SumOfValue - discreteKnapsackResult.SumOfValue) / (double)optimalResult.SumOfValue;
            return new List<string> {(error * 100) + "%"};
        }
    }
}
