﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models.discreteKnapsackProblem
{
    public class DiscreteKnapsack : IKnapsack
    {
        private readonly Int64 capacity;
        public Int64 Capacity => capacity;

        public Item[] Items { get; }

        public DiscreteKnapsack(Int64 capacity, Item[] items)
        {
            this.capacity = capacity;
            this.Items = items;
        }

        public String GetInfo()
        {
            String info = "";
            info += "Capacity: " + capacity + "\n";
            info += "Items:\n" +
                    "id\tweight\tvalue\n";
            int iter = 0;
            foreach (var item in Items)
            {
                info += iter + "\t" + item.GetInfo() + "\n";
                iter++;
            }
            return info;
        }
    }
}
