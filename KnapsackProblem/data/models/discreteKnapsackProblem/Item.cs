﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models.discreteKnapsackProblem
{
    public class Item : IItem
    {
        public int Weight { get; set; }
        public int Value { get; set; }

        public Item(int weight, int value)
        {
            this.Weight = weight;
            this.Value = value;
        }

        public Item()
        {
        }

        public String GetInfo()
        {
            String info = Weight + "\t" + Value;
            return info;
        }
    }
}
