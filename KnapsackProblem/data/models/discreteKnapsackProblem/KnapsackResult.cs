﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models.interfaces;

namespace KnapsackProblem.data.models.discreteKnapsackProblem
{
    public class KnapsackResult : IKnapsackResult
    {
        private readonly List<Item> items;
        private readonly Int64 sumOfValue;

        public List<Item> Items => items;
        public Int64 SumOfValue => sumOfValue;

        public KnapsackResult(List<Item> items, Int64 sumOfValue)
        {
            this.items = items;
            this.sumOfValue = sumOfValue;
        }

        public string GetShortInfo()
        {
            return "Sum of value: " + sumOfValue;
        }

        public string GetLongInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Sum of value: " + sumOfValue);
            if (items != null)
            {
                sb.Append("\nweight\tvalue\n");

                for (int i = 0; i < items.Count; i++)
                {
                    sb.Append(items[i].GetInfo() + "\n");
                }
            }

            return sb.ToString();
        }
    }
}
