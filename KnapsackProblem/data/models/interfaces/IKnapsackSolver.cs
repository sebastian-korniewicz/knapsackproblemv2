﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models.interfaces
{
    public interface IKnapsackSolver
    {
        IKnapsackResult Solve(IKnapsack discreteKnapsack);
        String GetName();
        List<string> GetCustomParametersNames();
        List<string> GetCustomParameters();
        void SetCustomParameters(Object[] objects);
    }
}
