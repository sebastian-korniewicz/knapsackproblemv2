﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models.interfaces
{
    public interface IKnapsackResult
    { 
        string GetShortInfo();
        string GetLongInfo();
    }
}
