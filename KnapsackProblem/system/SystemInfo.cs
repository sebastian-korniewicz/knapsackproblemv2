﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.system
{
    public class SystemInfo
    {
        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetPhysicallyInstalledSystemMemory(out long TotalMemoryInKilobytes);

        public static String getProcessorInfo()
        {
            RegistryKey processor_name = Registry.LocalMachine.OpenSubKey(@"Hardware\Description\System\CentralProcessor\0", RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.
            
        
            if (processor_name != null)
            {
                if (processor_name.GetValue("ProcessorNameString") != null)
                {
                    return (string) processor_name.GetValue("ProcessorNameString"); //Display processor ingo.
                }
                return "";
            }
            else
                return "";
        }

        public static String getRamInfo()
        {
            long memKb;
            GetPhysicallyInstalledSystemMemory(out memKb);
            return ((memKb / 1024) + " MB");
        }
    }
}
