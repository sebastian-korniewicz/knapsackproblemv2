﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KnapsackProblem.benchmark
{
    class MemChecker
    {
        private long memory = 0;
        private bool end = false;
        private int time = 1;

        public bool End { get => end; set => end = value; }

        public long GetMemory()
        {
            lock (this)
            {
                return memory;
            }
        }

        public MemChecker(long memory, int time)
        {
            this.memory = memory;
            this.time = time;
        }

        public void Run()
        {
            do
            {
                long currentMemory = GC.GetTotalMemory(false);
                lock (this)
                {
                    if (currentMemory > memory)
                        memory = currentMemory;
                }
                Thread.Sleep(time);
            } while (!End);
        }
    }
}
