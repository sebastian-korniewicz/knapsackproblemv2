﻿using KnapsackProblem.data.generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.validatorResult;
using KnapsackProblem.data.models;
using System.Diagnostics;
using KnapsackProblem.data.models.interfaces;
using KnapsackProblem.data.models.discreteKnapsackProblem;
using System.Threading;

namespace KnapsackProblem.benchmark
{
    class BenchmarkItem
    {
        private int itemAmount;
        private DataType dataType;
        private int percent;
        private int dataRange;
        private string[] timesList = null;
        private string[][] validatorList = null;
        private string[] memoryList = null;
        private string errorFile = null;
        private int timeOut = 0;
        private uint testAmount = 1;
        private IKnapsackSolver solver;
        private IKnapsackGenerator knapsackGenerator = null;
        private IKnapsackValidator knapsackValidator = null;
        private IKnapsack knapsack = null;
        private object[] parametersForGenerator = null;
        private object[] parametersForSolver = null;
        private static int seed = 155;
        private IKnapsackResult knapsackResult = null;

        public string[] MemoryList { get => memoryList; }
        public string[] TimesList { get => timesList; }

        public BenchmarkItem(
            IKnapsackSolver solver,
            IKnapsackGenerator discreteKnapsackGenerator,
            IKnapsackValidator knapsackValidator,
            uint testAmount,
            int itemAmount,
            int timeOut,
            Object[] parametersForGenerator,
            Object[] parametersForSolver,
            bool memTest,
            string errorFile)
        {  
            //Console.WriteLine("seed " + seed);
            //if (seed == 203)
            //    Console.WriteLine("2");
            //discreteKnapsackGenerator.SetSeed(seed++);
            knapsack = discreteKnapsackGenerator.Generate((int)itemAmount, parametersForGenerator);
            this.solver = solver;
            this.itemAmount = itemAmount;
            this.testAmount = testAmount;
            this.parametersForSolver = parametersForSolver;
            this.parametersForGenerator = parametersForGenerator;
            this.timeOut = timeOut;
            this.errorFile = errorFile;
            timesList = new String[testAmount];

            if(memTest)
                memoryList = new String[testAmount];
            if (knapsackValidator!=null)
            {
                validatorList = new String[testAmount][];
                this.knapsackValidator = knapsackValidator;
            }
        }

        public void RunTest()
        {
            // cleanup memoryList

            
            if (validatorList!=null)
                knapsackValidator.SetKnapsack(knapsack);
            for (int i = 0; i < testAmount; i++)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Type solverType = Type.GetType(this.solver.ToString());
                Stopwatch stopwatch = new Stopwatch();
                long startMemory = GC.GetTotalMemory(false);
                MemChecker memChecker = new MemChecker(startMemory, 10);
                // prepare solver
                IKnapsackSolver solver = (IKnapsackSolver)Activator.CreateInstance(solverType, true);
                if (parametersForSolver != null)
                {
                    solver.SetCustomParameters(parametersForSolver);
                }
                
                new Thread(memChecker.Run).Start();
                try
                {
                    Thread t = new Thread(RunSolver);
                    stopwatch.Start();
                    t.Start();
                    if (t.Join(TimeSpan.FromSeconds(timeOut)))
                    {
                        stopwatch.Stop();
                        memChecker.End = true;
                        long endMemory = memChecker.GetMemory();
                        long totalMemory = GC.GetTotalMemory(false);
                        if (totalMemory > endMemory)
                            endMemory = totalMemory;
                        TimesList[i] = ((stopwatch.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L))) / 1000d)
                            .ToString();
                        if (memoryList != null)
                        {
                            memoryList[i] = (((double) (endMemory - startMemory)) / 1024).ToString();
                        }

                        if (validatorList != null)
                            validatorList[i] = knapsackValidator.ValidateSolution(knapsackResult).ToArray();
                    }
                    else
                    {
                        t.Abort();
                        memChecker.End = true;
                        TimesList[i] = "timeOut";
                        if (validatorList != null)
                        {
                            validatorList[i] = new string[knapsackValidator.ColumnsInfo().Count];

                            for (int j = 0; j < knapsackValidator.ColumnsInfo().Count; j++)

                                validatorList[i][j] = "#";
                        }

                        if (memoryList != null)
                            memoryList[i] = "#";
                    }

                }
                catch (Exception e)
                {
                    memChecker.End = true;
                    solver = null;
                    GC.Collect();
                    Console.WriteLine(e.Message);
                    TimesList[i] = "#";
                    if (validatorList != null)
                    {
                        validatorList[i] = new string[knapsackValidator.ColumnsInfo().Count];
                        for (int j = 0; j < knapsackValidator.ColumnsInfo().Count; j++)
                            validatorList[i][j] = "#";
                    }
                    if (memoryList != null)
                        memoryList[i] = "memError";

                    if (errorFile!=null)
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(errorFile, true))
                        {
                            file.WriteLine(e.ToString());
                        }
                    }
                }
            }
        }

        private void RunSolver()
        {
            try
            {
                knapsackResult = solver.Solve(knapsack);
            }
            catch (OutOfMemoryException e)
            {

            }
            catch (ThreadAbortException e)
            {

            }
        }
        public String exportCSV()
        {
            StringBuilder sb = new StringBuilder();
            if (parametersForSolver != null)
                foreach (var param in parametersForSolver)
                    sb.Append(param + ";");
            sb.Append(itemAmount + ";");
            foreach (var param in parametersForGenerator)
                sb.Append(param + ";");
            foreach (var time in TimesList)
                sb.Append(time + ";");
            if(memoryList!=null)
                foreach (var mem in memoryList)
                    sb.Append(mem + ";");
            if (validatorList != null)
                foreach (var validationResult in validatorList)
                {
                    if(validationResult!=null)
                        foreach (var singleResult in validationResult)
                            sb.Append(singleResult + ";");
                    else
                    {
                        for (int j = 0; j < knapsackValidator.ColumnsInfo().Count; j++)
                            sb.Append("#;");
                    }
                }

            sb.Length--;
            return sb.ToString();
        }
    }
}
