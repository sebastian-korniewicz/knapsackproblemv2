using System;
using System.Collections.Generic;
using System.Text;
using KnapsackProblem.data.generator;
using KnapsackProblem.data.models;
using KnapsackProblem.validatorResult;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KnapsackProblem.data.models.interfaces;
using KnapsackProblem.statistic;
using KnapsackProblem.system;

namespace KnapsackProblem.benchmark
{
    public class Benchmark
    {
        private IKnapsackSolver solver;

        public IKnapsackSolver Solver
        {
            get => solver;
            set => solver = value;
        }

        public IKnapsackValidator KnapsackValidator { get; set; }
        public IKnapsackGenerator KnapsackGenerator { get; set; }

        public List<object[]> ParametersForGenerator { get; set; }
        public List<object[]> ParametersForSolver { get; set; }
        public List<string> NamesOfParametersForGeneretor { get; set; }
        public List<int> ItemAmount { get; set; }
        public bool Memory { get; set; } = false;
        public bool MemoryStatistic { get; set; } = false;
        public bool TimeStatistic { get; set; } = false;
        public bool PrintResultsInConsole { get; set; } = true;
        public bool BreakBenchmark { get; set; } = true;
        public int TimeOut { get; set; } = 5;
        public uint AmountOfTestsInstance { get; set; } = 2;
        public uint AmountOfTestsGeneratorParameters { get; set; } = 1;
        public string CsvFile { get; set; } = null;
        public string ErrorFile { get; set; } = null;
        


        public Benchmark()
        {
            
        }

        private void CheckParameters()
        {
            if(solver == null)
                throw new Exception("solver was not selected");
            if (solver == null)
                throw new Exception("generator was not selected");
            if (ParametersForGenerator == null || ParametersForGenerator.Count==0)
                throw new Exception("parameters for generator was not selected");
            if(ItemAmount == null)
                throw new Exception("number of items was not selected");
            object[] parametersForGenerator = new object[ParametersForGenerator.Count];
            for (int i = 0; i < ParametersForGenerator.Count; i++)
                parametersForGenerator[i] = ParametersForGenerator[i][0];
            object[] parametersForSolver = null;
            if (ParametersForSolver != null)
            {
                parametersForSolver = new object[ParametersForSolver.Count];
                for (int i = 0; i < ParametersForGenerator.Count; i++)
                    parametersForSolver[i] = ParametersForSolver[i][0];
            }

            BenchmarkItem benchmarkItem = new BenchmarkItem(solver, KnapsackGenerator, KnapsackValidator, 1, 10, TimeOut, parametersForGenerator, parametersForSolver, Memory, null);
            benchmarkItem.RunTest();

        }
         
        private void RunTest(object[] parametersForSolver)
        {
            int[] iters = new int[ParametersForGenerator.Count + 1];
            for (int i = 1; i < iters.Length; i++)
            {
                iters[i] = 0;
            }

            iters[0] = -1;

            int all = 1;
            foreach (var parameterElement in ParametersForGenerator)
            {
                all *= parameterElement.Length;
            }

            for (int iterSum = 0; iterSum < all; iterSum++)
            {
                int iterParam = 0;
                iters[iterParam]++;
                while (iterParam < ParametersForGenerator.Count && ParametersForGenerator[iterParam].Length == iters[iterParam])
                {
                    iters[iterParam++] = 0;
                    iters[iterParam]++;
                }
                Object[] parametersArray = new Object[ParametersForGenerator.Count];
                for (int i = 0; i < parametersArray.Length; i++)
                    parametersArray[i] = ParametersForGenerator[i][iters[i]];

                for (int i = 0; i < ItemAmount.Count; i++)
                {
                    for (int j = 0; j< AmountOfTestsGeneratorParameters; j++) {
                        BenchmarkItem benchmarkItem = new BenchmarkItem(solver, KnapsackGenerator, KnapsackValidator, AmountOfTestsInstance, ItemAmount[i], TimeOut, parametersArray, parametersForSolver, Memory, ErrorFile);
                        try
                        {
                            benchmarkItem.RunTest();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                            if (ErrorFile != null)
                            {
                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(ErrorFile, true))
                                {
                                    file.WriteLine(e.ToString());
                                }
                            }
                        }
                        if (CsvFile != null)
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(CsvFile, true))
                            {
                                file.Write(benchmarkItem.exportCSV());
                                if (MemoryStatistic && benchmarkItem.MemoryList != null)
                                {
                                    file.Write(";");
                                    var memList = benchmarkItem.MemoryList;
                                    List<double> doubles = new List<double>();
                                    foreach (var mem in memList)
                                    {
                                        double d;
                                        if (Double.TryParse(mem, out d))
                                            doubles.Add(d);
                                    }
                                    file.Write(Statistic.GetAverage(doubles) + ";" + Statistic.GetStdDev(doubles));
                                }
                                if (TimeStatistic && benchmarkItem.TimesList != null)
                                {
                                    file.Write(";");
                                    var timeList = benchmarkItem.TimesList;
                                    List<double> doubles = new List<double>();
                                    foreach (var mem in timeList)
                                    {
                                        double d;
                                        if (Double.TryParse(mem, out d))
                                            doubles.Add(d);
                                    }
                                    file.Write(Statistic.GetAverage(doubles) + ";" + Statistic.GetStdDev(doubles));
                                }
                                file.Write("\n");
                            }
                        }
                        Console.WriteLine(benchmarkItem.exportCSV());
                        if (parametersForSolver != null)
                            foreach (var VARIABLE in parametersForSolver)
                            {
                                Console.Write(VARIABLE.ToString() + "\t");
                            }
                        Console.Write(ItemAmount[i] + "\t");
                        foreach (var VARIABLE in parametersArray)
                        {
                            Console.Write(VARIABLE.ToString() + "\t");
                        }
                        Console.WriteLine("");
                    }
                }
            }
        }

        public void RunTests(IKnapsackSolver solver)
        {
            this.solver = solver;
            CheckParameters();

            var columnsNames = BuildHeader();

            Console.WriteLine("system info");
            Console.WriteLine("processor\t" + SystemInfo.getProcessorInfo());
            Console.WriteLine("ram\t" + SystemInfo.getRamInfo() + "\n");
            Console.WriteLine("start time\t" + DateTime.Now);
            Console.WriteLine("algorithm\t" + solver.GetName());
            foreach (var VARIABLE in columnsNames)
            {
                Console.Write(VARIABLE + "\t");
            }
            Console.WriteLine();

            if (CsvFile!=null)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(CsvFile, true))
                {
                    file.WriteLine("system info");
                    file.WriteLine("processor;" + SystemInfo.getProcessorInfo());
                    file.WriteLine("ram;" + SystemInfo.getRamInfo());
                    file.WriteLine("algorithm;" + solver.GetName());
                    file.WriteLine("start;" + DateTime.Now);
                    file.WriteLine("");
                    foreach (var VARIABLE in columnsNames)
                    {
                        file.Write(VARIABLE + ";");
                    }
                    file.Write("\n");
                }
            }

            if (ParametersForSolver != null)
            {
                int[] iters = new int[ParametersForSolver.Count + 1];
                for (int i = 1; i < iters.Length; i++)
                {
                    iters[i] = 0;
                }

                iters[0] = -1;

                int all = 1;
                foreach (var parameterElement in ParametersForSolver)
                {
                    all *= parameterElement.Length;
                }

                for (int iterSum = 0; iterSum < all; iterSum++)
                {
                    int iterParam = 0;
                    iters[iterParam]++;
                    while (iterParam < ParametersForSolver.Count &&
                           ParametersForSolver[iterParam].Length == iters[iterParam])
                    {
                        iters[iterParam++] = 0;
                        iters[iterParam]++;
                    }

                    Object[] parametersArray = new Object[ParametersForSolver.Count];
                    for (int i = 0; i < parametersArray.Length; i++)
                        parametersArray[i] = ParametersForSolver[i][iters[i]];
                    RunTest(parametersArray);
                }
            }
            else
            {
                RunTest(null);
            }
            Console.WriteLine("end time\t" + DateTime.Now);
            if (CsvFile != null)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(CsvFile, true))
                {
                    file.WriteLine();
                    file.WriteLine("end;" + DateTime.Now);
                }
            }
        }

        private List<string> BuildHeader()
        {
            List<string> header = new List<string>();
            var x = solver.GetCustomParametersNames();
            if(x!=null)
                foreach (var VARIABLE in x)
                    header.Add(VARIABLE);

            header.Add("item amount");

            var generatorParams = KnapsackGenerator.GetParametersNames();
            if (generatorParams != null)
                for (int i = 0; i < generatorParams.Count; i++)
                    header.Add(generatorParams[i]);

            
            for (int i = 0; i < AmountOfTestsInstance; i++)
                header.Add("time " + (i + 1));

            if (Memory)
                for(int i = 0; i < AmountOfTestsInstance; i++)
                    header.Add("memory " + (i+1));
            if (KnapsackValidator!=null)
                for (int i = 0; i < AmountOfTestsInstance; i++)
                {
                    var validatorInfo = KnapsackValidator.ColumnsInfo();
                    foreach (var column in validatorInfo)
                    {
                        header.Add(column + " " + (i+1));
                    }
                }

            if(MemoryStatistic)
            {
                header.Add("average memory");
                header.Add("std dev memory");
            }
            if(TimeStatistic)
            {
                header.Add("average time");
                header.Add("std dev time");
            }
            return header;
        }
    }
}
