﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.statistic
{
    public class Statistic
    {
        public static double GetStdDev(List<double> doubles)
        {
            double average = doubles.Average();
            double sumOfSquaresOfDifferences = doubles.Select(val => (val - average) * (val - average)).Sum();
            double sd = System.Math.Sqrt(sumOfSquaresOfDifferences / doubles.Count);
            return sd;
        }
        public static double GetAverage(List<double> doubles)
        {
            return doubles.Average();
        }
    }
}
