﻿using KnapsackProblem.algorithms;
using KnapsackProblem.benchmark;
using KnapsackProblem.data.generator;
using KnapsackProblem.validatorResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPConstCapacityBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            DynamicProgramming dynamicProgramming = new DynamicProgramming();
            Benchmark benchmark = new Benchmark();
            benchmark.Memory = true;
            benchmark.MemoryStatistic = true;
            benchmark.TimeStatistic = true;
            benchmark.TimeOut = 30;
            benchmark.CsvFile = "dp2.csv";
            benchmark.AmountOfTestsInstance = 10;
            benchmark.AmountOfTestsGeneratorParameters = 3;
            List<int> itemAmount = new List<int> { 50, 100, 150 };
            benchmark.ItemAmount = itemAmount;
            benchmark.KnapsackGenerator = new DiscreteKnapsackGeneratorConstCapacity();
            benchmark.KnapsackValidator = new DeviationValidatorCombo();
            object[] range = { 100, 1000 };
            object[] capacity = { 50L, 1000L };
            object[] dataTypes =
            {
                DataType.UncorrelatedDataInstances,
                DataType.WeaklyCorrelatedInstances
            };
            benchmark.ParametersForGenerator = new List<object[]> { range, capacity, dataTypes };
            benchmark.RunTests(dynamicProgramming);
        }
    
    }
}
